Before being able to use this programm you need to extract and copy some files to the correct Locations:

	1. Using the bbaTool extract the main "data.bba" of the game
	1.1 You can also extract the files that are loaded later and copy them over
	2. Copy the extracted "graphics" folder to "data/graphics"
	3. Copy the extracted "config" folder to "data/config"


In order to use the .obj Import functionality you will also have to download ImageMagick

	1. Download ImageMagick from www.imagemagick.org
	2. extract ImageMagick into the "ImageMagick" folder
	2.1 magick.exe should be directly in the "ImageMagick" folder
