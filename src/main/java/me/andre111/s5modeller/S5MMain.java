package me.andre111.s5modeller;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;

import me.andre111.s5modeller.gui.S5MGui;

//TODO: A way of creating a "fake class" with an own description file 
//      (for example animFrame, containing time,ori,trans,prevFrame) 
//      to make describing files simpler (the repeat until eof has
//      worked surprisingly well so far, but it cannot fit everything)
//      Would also make listing them in an orderly fashion a bit
//      easier (in contrast to the giant list it currently creates)
public class S5MMain {
	public static void main(String[] args) {
		try {
			addNativeLibraryDirInternal("./natives/");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		new S5MGui();
	}
	
	private static void addNativeLibraryDirInternal(String s) throws IOException {
		try {
			// This enables the java.library.path to be modified at runtime
			// From a Sun engineer at http://forums.sun.com/thread.jspa?threadID=707176
			//
			Field field = ClassLoader.class.getDeclaredField("usr_paths");
			field.setAccessible(true);
			String[] paths = (String[])field.get(null);
			for (int i = 0; i < paths.length; i++) {
				if (s.equals(paths[i])) {
					return;
				}
			}
			String[] tmp = new String[paths.length+1];
			System.arraycopy(paths,0,tmp,0,paths.length);
			tmp[paths.length] = s;
			field.set(null,tmp);
			System.setProperty("java.library.path", System.getProperty("java.library.path") + File.pathSeparator + s);
		} catch (IllegalAccessException e) {
			throw new IOException("Failed to get permissions to set library path");
		} catch (NoSuchFieldException e) {
			throw new IOException("Failed to get field handle to set library path");
		}
	}
}
