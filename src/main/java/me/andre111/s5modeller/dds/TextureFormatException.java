package me.andre111.s5modeller.dds;
 
public class TextureFormatException extends Exception {
     
    private static final long serialVersionUID = 24L;
     
    public TextureFormatException() {
        super();
    }
     
    public TextureFormatException(String s) {
        super(s);            
    }
}
