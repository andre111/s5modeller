package me.andre111.s5modeller.display;


import me.andre111.s5modeller.gui.S5MGui;
import me.andre111.s5modeller.model.Animation;
import me.andre111.s5modeller.model.Model;
import me.andre111.s5modeller.model.Skeleton;
import me.andre111.s5modeller.model.Skeleton.Bone;
import me.andre111.s5modeller.rwbs.RWSection;
import me.andre111.s5modeller.rwbs.data.RWFloat;
import me.andre111.s5modeller.rwbs.data.RWFloatArray2D;
import me.andre111.s5modeller.rwbs.data.RWInteger;
import me.andre111.s5modeller.rwbs.util.RWUtil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JDialog;
import javax.swing.JTextArea;

import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.GLU;
import org.lwjgl.util.vector.Matrix4f;

public class S5MDisplay implements Runnable {
	public static final int WIDTH = 1280;
	public static final int HEIGHT = WIDTH * 9 / 16;

	private boolean running;

	private int fps = 0;
	private long lastFPS = 0;
	private long lastFrame = 0;

	private boolean wireframe = false;

	private EulerCamera camera;

	private S5MGui parentGui;

	private RWSection toLoad;
	private RWSection section;
	private Model newModel;
	private Skeleton skeleton;
	private Animation animation;
	
	private float animTime = 0;
	
	public S5MDisplay(S5MGui parent) {
		parentGui = parent;
	}

	public void start() {
		running = true;
		Thread t = new Thread(this, "Display");
		t.start();
	}

	public void stop() {
		running = false;
	}

	@Override
	public void run() {
		try {
			setDisplayMode(WIDTH, HEIGHT, false);
			Display.setTitle("Display");
			Display.create();
		} catch (LWJGLException e) {
			e.printStackTrace();
			System.exit(0);
		}

		initGL();
		getDelta();
		lastFPS = getTime();

		camera = new EulerCamera.Builder().setPosition(-6, 2, -6).setRotation(0, 160, 0).setAspectRatio(WIDTH/((float) HEIGHT)).setFieldOfView(60).build();

		while(!Display.isCloseRequested() && running) {
			int delta = getDelta();

			pollInput();
			update(delta);
			updateFPS();
			renderGL();

			Display.update();
			Display.sync(60); //cap fps to 60
		}

		Display.destroy();
		running = false;
	}

	public void initGL() {
		GL11.glViewport(0, 0, WIDTH, HEIGHT); // Reset The Current Viewport
		GL11.glMatrixMode(GL11.GL_PROJECTION); // Select The Projection Matrix
		GL11.glLoadIdentity(); // Reset The Projection Matrix
		GLU.gluPerspective(45.0f, ((float) WIDTH / (float) HEIGHT), 0.1f, 100.0f); // Calculate The Aspect Ratio Of The Window
		GL11.glMatrixMode(GL11.GL_MODELVIEW); // Select The Modelview Matrix
		GL11.glLoadIdentity(); // Reset The Modelview Matrix

		GL11.glShadeModel(GL11.GL_SMOOTH); // Enables Smooth Shading
		GL11.glClearColor(0.0f, 0.0f, 0.0f, 0.0f); // Black Background
		GL11.glClearDepth(1.0f); // Depth Buffer Setup
		GL11.glEnable(GL11.GL_DEPTH_TEST); // Enables Depth Testing
		GL11.glDepthFunc(GL11.GL_LEQUAL); // The Type Of Depth Test To Do
		//GL11.glHint(GL11.GL_PERSPECTIVE_CORRECTION_HINT, GL11.GL_NICEST); // Really Nice Perspective Calculations

		GL11.glEnable(GL11.GL_CULL_FACE);
	}

	public void setModel(RWSection section) {
		toLoad = section;
	}

	public void applySkeleton(Skeleton s) {
		if(s.getRootBone()==null) {
			skeleton = null;
			return;
		}

		skeleton = s;
	}
	
	public void applyAnimation(Animation a) {
		animation = a;
	}

	public void update(int delta) {
		if(toLoad!=null) {
			toLoad.printInfo(true, false, "");
			newModel = new Model(toLoad);
			section = toLoad;
			toLoad = null;
		}
		
		animTime += delta / 1000f;
	}

	public void renderGL() {
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT); // Clear The Screen And The Depth Buffer
		GL11.glLoadIdentity(); // Reset The View

		GL11.glDepthFunc(GL11.GL_LEQUAL);

		// Apply the camera position and orientation to the scene
		camera.applyTranslations();

		GL11.glScalef(0.01f, 0.01f, 0.01f);
		GL11.glRotatef(-90f, 1f, 0f, 0f);

		//TODO - stuff like snow and tree movement are defined with shaders in graphics/effects
		//TODO - hack to show leaves somewhat correctly, correct alpha would need depth sorting
		GL11.glEnable(GL11.GL_ALPHA_TEST);
		GL11.glAlphaFunc(GL11.GL_GREATER, 0.2f);
		//TODO - this makes leaves not work with correct overlapping, would need depth sorting, low priority right now(because only visual defects)
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

		if(newModel!=null) {
			newModel.draw((vertex, index) -> modifyVertex(vertex, index));
			if(wireframe) {
				newModel.drawWireframe((vertex, index) -> modifyVertex(vertex, index));
			}
		}

		//TODO: Just for testing
		if(animation!=null) {
			updateAnimation();
		}
		if(skeleton!=null) {
			GL11.glTranslatef(200f, 0f, 0f);

			GL11.glLineWidth(2f);
			GL11.glColor3f(1, 1, 1);

			testBoneDisplay(skeleton.getRootBone(), null);
		}

		GL11.glDisable(GL11.GL_ALPHA_TEST);
		GL11.glDisable(GL11.GL_BLEND);
	}
	
	private void updateAnimation() {
		RWSection frameList = RWUtil.getSection(section, 0x000E);
		RWSection struct = RWUtil.getSection(frameList, 0x0001);
		
		int frameCount = ((RWInteger) RWUtil.getData(struct, "frameCount")).getInteger();
		
		//find root HAnim
		RWSection rootHAnim = null;
		int boneCount = 0;
		for(int f=0; f<frameCount; f++) {
			RWSection extension = RWUtil.getSection(frameList, 0x0003, f);
			RWSection hanim = RWUtil.getSection(extension, 0x011E);
			if(hanim!=null) {
				boneCount = ((RWInteger) RWUtil.getData(hanim, "boneCount")).getInteger();
				if(boneCount!=0) {
					rootHAnim = hanim;
					break;
				}
			}
		}
		
		//map bone ids
		Map<Integer, Integer> boneNumberToBoneID = new HashMap<>();
		Map<Integer, Integer> boneIDToFrameIndex = new HashMap<>();
		for(int i=0; i<boneCount; i++) {
			int boneID = ((RWInteger) RWUtil.getData(rootHAnim, "boneID", i)).getInteger();
			int boneNumber = ((RWInteger) RWUtil.getData(rootHAnim, "boneNumber", i)).getInteger();
			
			if(boneNumber!=i) {
				System.out.println("Info: Found bone that is out of order? Found "+boneNumber+" expected "+i);
			}
			
			boneNumberToBoneID.put(boneNumber, boneID);
			//System.out.println("Mapping Bone "+boneNumber+" to ID "+boneID);
			
			//find fitting frame
			boolean foundFrame = false;
			for(int f=0; f<frameCount; f++) {
				RWSection extension = RWUtil.getSection(frameList, 0x0003, f);
				RWSection hanim = RWUtil.getSection(extension, 0x011E);
				if(hanim!=null) {
					int frameBoneID = ((RWInteger) RWUtil.getData(hanim, "ownBoneID")).getInteger();
					if(frameBoneID==boneID) {
						boneIDToFrameIndex.put(boneID, f);
						//System.out.println("Mapping Bone ID "+boneID+" to frame "+f);
						foundFrame = true;
						break;
					}
				}
			}
			
			if(!foundFrame) {
				System.out.println("Warning: no matching frame for bone number "+boneNumber+" with id "+boneID+"!");
			}
		}
		
		//TODO: This currently does not work correctly (for skinned characters, for buildings it is almost correct), 
		//      but I cannot figure out where the error lies (maybe in getBoneMatrix?)
		
		//calculate offset based on bone id
		int testOffset = 0;
		if(animation.getBaseBoneID()>=0) {
			for(Map.Entry<Integer, Integer> e : boneNumberToBoneID.entrySet()) {
				if(e.getValue()==animation.getBaseBoneID()) {
					testOffset = e.getKey();
				}
			}
		}
		
		for(int i=0; i<animation.getBoneCount(); i++) {
			if(boneNumberToBoneID.containsKey(i+testOffset)) {
				float[][] mat = animation.getBoneMatrix(i, animTime%animation.getMaxTime());
				
				int boneID = boneNumberToBoneID.get(i+testOffset);
				if(boneIDToFrameIndex.containsKey(boneID)) {
					int frameIndex = boneIDToFrameIndex.get(boneID);
					
					//find fitting frame
					RWFloatArray2D rotMat = (RWFloatArray2D) RWUtil.getData(struct, "rotationMatrix", frameIndex);
					RWFloat offX = (RWFloat) RWUtil.getData(struct, "coordinateOffsetX", frameIndex);
					RWFloat offY = (RWFloat) RWUtil.getData(struct, "coordinateOffsetY", frameIndex);
					RWFloat offZ = (RWFloat) RWUtil.getData(struct, "coordinateOffsetZ", frameIndex);
					
					//apply transformation
					rotMat.getArray()[0][0] = mat[0][0];
					rotMat.getArray()[0][1] = mat[0][1];
					rotMat.getArray()[0][2] = mat[0][2];
					rotMat.getArray()[1][0] = mat[1][0];
					rotMat.getArray()[1][1] = mat[1][1];
					rotMat.getArray()[1][2] = mat[1][2];
					rotMat.getArray()[2][0] = mat[2][0];
					rotMat.getArray()[2][1] = mat[2][1];
					rotMat.getArray()[2][2] = mat[2][2];
					
					offX.setFloat(mat[3][0]);
					offY.setFloat(mat[3][1]);
					offZ.setFloat(mat[3][2]);
				} else {
					System.out.println("IW: missing map for boneID "+boneID);
				}
			} else {
				System.out.println("Warning: missing bone with number "+i+" for animation!");
			}
		}
	}

	private Matrix4f getBoneTransformation(Bone bone, Matrix4f parentMatrix) {
		//calculate (absolute) transformation matrix of bone
		Matrix4f transformation = new Matrix4f();
		transformation.setIdentity();

		if(bone.getFrameRotationMatrix()!=null) {
			Matrix4f matrix = new Matrix4f();
			matrix.setZero();
			
			//TODO: Tests
			matrix.m00 = bone.getFrameRotationMatrix()[0][0];
			matrix.m01 = bone.getFrameRotationMatrix()[0][1];
			matrix.m02 = bone.getFrameRotationMatrix()[0][2];
			matrix.m03 = 0;
			matrix.m10 = bone.getFrameRotationMatrix()[1][0];
			matrix.m11 = bone.getFrameRotationMatrix()[1][1];
			matrix.m12 = bone.getFrameRotationMatrix()[1][2];
			matrix.m13 = 0;
			matrix.m20 = bone.getFrameRotationMatrix()[2][0];
			matrix.m21 = bone.getFrameRotationMatrix()[2][1];
			matrix.m22 = bone.getFrameRotationMatrix()[2][2];
			matrix.m23 = 0;
			matrix.m30 = bone.getFrameOffset()[0];
			matrix.m31 = bone.getFrameOffset()[1];
			matrix.m32 = bone.getFrameOffset()[2];
			matrix.m33 = 1;
		
			
			if(parentMatrix!=null) {
				Matrix4f.mul(parentMatrix, matrix, transformation);
			} else {
				transformation = matrix;
			}
		}

		return transformation;
	}

	private void testBoneDisplay(Bone bone, Matrix4f parentMatrix) {
		Matrix4f transformation = getBoneTransformation(bone, parentMatrix);
		
		//update runtime matrix (used for skinning)
		bone.getCurrentMatrix()[0][0] = transformation.m00;
		bone.getCurrentMatrix()[0][1] = transformation.m01;
		bone.getCurrentMatrix()[0][2] = transformation.m02;
		bone.getCurrentMatrix()[0][3] = transformation.m03;
		bone.getCurrentMatrix()[1][0] = transformation.m10;
		bone.getCurrentMatrix()[1][1] = transformation.m11;
		bone.getCurrentMatrix()[1][2] = transformation.m12;
		bone.getCurrentMatrix()[1][3] = transformation.m13;
		bone.getCurrentMatrix()[2][0] = transformation.m20;
		bone.getCurrentMatrix()[2][1] = transformation.m21;
		bone.getCurrentMatrix()[2][2] = transformation.m22;
		bone.getCurrentMatrix()[2][3] = transformation.m23;
		bone.getCurrentMatrix()[3][0] = transformation.m30;
		bone.getCurrentMatrix()[3][1] = transformation.m31;
		bone.getCurrentMatrix()[3][2] = transformation.m32;
		bone.getCurrentMatrix()[3][3] = transformation.m33;

		//display line with transformation
		GL11.glPushMatrix();

		FloatBuffer buf = BufferUtils.createFloatBuffer(16);
		transformation.store(buf);
		buf.rewind();
		GL11.glMultMatrix(buf);

		GL11.glBegin(GL11.GL_LINES);
		GL11.glVertex3f(0, 0, 0);
		GL11.glVertex3f(10, 0, 0);
		GL11.glEnd();

		GL11.glPopMatrix();

		//display children
		for(Bone child : bone.getChildren()) {
			testBoneDisplay(child, transformation);
		}
	}
	
	private Bone[] vertexBones = new Bone[4];
	private float[] modifyVertex(float[] vertex, int index) {
		if(skeleton!=null) {
			//find bones that apply
			int vertexBoneCount = 0;
			List<Bone> bones = new ArrayList<>();
			bones.add(skeleton.getRootBone());
			while(!bones.isEmpty()) {
				Bone bone = bones.remove(bones.size()-1);
				bones.addAll(bone.getChildren());
				
				if(bone.modifiesVertex(index)) {
					vertexBones[vertexBoneCount] = bone;
					vertexBoneCount++;
				}
			}
			if(vertexBoneCount==0) {
				return vertex;
			}
			
			float[] poseVertex = new float[4];
			for(int i=0; i<vertexBoneCount; i++) {
				//transform vertex to bone space
				vertexBones[i].getInverseBoneMatrix()[0][3] = 0; //TODO: Why is this needed?
				vertexBones[i].getInverseBoneMatrix()[1][3] = 0; //TODO: Why is this needed?
				vertexBones[i].getInverseBoneMatrix()[2][3] = 0; //TODO: Why is this needed?
				vertexBones[i].getInverseBoneMatrix()[3][3] = 1; //TODO: Why is this needed?
				float[] boneSpaceVertex = multVecMat(vertex, vertexBones[i].getInverseBoneMatrix());
				
				//transform bone space vertex to pose
				float[] boneVertex = multVecMat(boneSpaceVertex, vertexBones[i].getCurrentMatrix());
				float weight = vertexBones[i].getVertexWeight(index);
				
				poseVertex[0] += weight * boneVertex[0];
				poseVertex[1] += weight * boneVertex[1];
				poseVertex[2] += weight * boneVertex[2];
				poseVertex[3] += weight * boneVertex[3];
			}
			
			poseVertex[0] /= poseVertex[3];
			poseVertex[1] /= poseVertex[3];
			poseVertex[2] /= poseVertex[3];
			poseVertex[3] = 1;
			
			return poseVertex;
		}
		
		return vertex;
	}
	
	private float[] multVecMat(float[] vertex, float[][] mat4) {
		float[] newVertex = new float[4];
		
		//multiply
		newVertex[0] = mat4[0][0] * vertex[0] + mat4[1][0] * vertex[1] + mat4[2][0] * vertex[2] + mat4[3][0] * vertex[3];
		newVertex[1] = mat4[0][1] * vertex[0] + mat4[1][1] * vertex[1] + mat4[2][1] * vertex[2] + mat4[3][1] * vertex[3];
		newVertex[2] = mat4[0][2] * vertex[0] + mat4[1][2] * vertex[1] + mat4[2][2] * vertex[2] + mat4[3][2] * vertex[3];
		newVertex[3] = mat4[0][3] * vertex[0] + mat4[1][3] * vertex[1] + mat4[2][3] * vertex[2] + mat4[3][3] * vertex[3];
		
		//
		newVertex[0] /= newVertex[3];
		newVertex[1] /= newVertex[3];
		newVertex[2] /= newVertex[3];
		newVertex[3] = 1;

		return newVertex;
	}

	public void pollInput() {
		if (Mouse.isButtonDown(0)) {
			camera.processMouse(1, 80, -80);
		}
		camera.processKeyboard(16, 1);

		if(newModel!=null) {
			if(Mouse.isButtonDown(1)) {
				newModel.setSelection(performSelection(Mouse.getX(), Mouse.getY()));
			}

			boolean reloadGui = false;
			float speed = 1;
			float rotSpeed = 0.25f;
			float scale = 0.01f;
			if(Keyboard.isKeyDown(Keyboard.KEY_LCONTROL)) {
				speed *= 10;
				rotSpeed *= 10;
				scale *= 10;
			}
			if(Keyboard.isKeyDown(Keyboard.KEY_TAB)) {
				speed /= 10;
				rotSpeed /= 10;
				scale /= 10;
			}

			if(Keyboard.isKeyDown(Keyboard.KEY_NUMPAD8) || Keyboard.isKeyDown(Keyboard.KEY_8)) {
				newModel.move(speed, 0, 0);
				reloadGui = true;
			}
			if(Keyboard.isKeyDown(Keyboard.KEY_NUMPAD2) || Keyboard.isKeyDown(Keyboard.KEY_2)) {
				newModel.move(-speed, 0, 0);
				reloadGui = true;
			}
			if(Keyboard.isKeyDown(Keyboard.KEY_NUMPAD4) || Keyboard.isKeyDown(Keyboard.KEY_4)) {
				newModel.move(0, speed, 0);
				reloadGui = true;
			}
			if(Keyboard.isKeyDown(Keyboard.KEY_NUMPAD6) || Keyboard.isKeyDown(Keyboard.KEY_6)) {
				newModel.move(0, -speed, 0);
				reloadGui = true;
			}

			if(Keyboard.isKeyDown(Keyboard.KEY_NUMPAD9) || Keyboard.isKeyDown(Keyboard.KEY_9)) {
				newModel.move(0, 0, speed);
				reloadGui = true;
			}
			if(Keyboard.isKeyDown(Keyboard.KEY_NUMPAD3) || Keyboard.isKeyDown(Keyboard.KEY_3)) {
				newModel.move(0, 0, -speed);
				reloadGui = true;
			}

			if(Keyboard.isKeyDown(Keyboard.KEY_NUMPAD7) || Keyboard.isKeyDown(Keyboard.KEY_7)) {
				newModel.rotate((float) (rotSpeed*Math.PI/180));
				reloadGui = true;
			}
			if(Keyboard.isKeyDown(Keyboard.KEY_NUMPAD1) || Keyboard.isKeyDown(Keyboard.KEY_1)) {
				newModel.rotate((float) (-rotSpeed*Math.PI/180));
				reloadGui = true;
			}

			if(Keyboard.isKeyDown(Keyboard.KEY_ADD)) {
				newModel.scale(1+scale);
				reloadGui = true;
			}
			if(Keyboard.isKeyDown(Keyboard.KEY_SUBTRACT)) {
				newModel.scale(1-scale);
				reloadGui = true;
			}

			if(reloadGui) {
				parentGui.recreate();
			}
		}

		//Keyboard events
		while(Keyboard.next()) {
			if(Keyboard.getEventKeyState()) {
				keyPressed(Keyboard.getEventKey());
			} else {
				keyReleased(Keyboard.getEventKey());
			}
		}
	}

	public int performSelection(int x, int y) {
		IntBuffer selBuffer = ByteBuffer.allocateDirect(1024).order(ByteOrder.nativeOrder()).asIntBuffer();
		int[] buffer = new int[256];

		IntBuffer viewBuffer = ByteBuffer.allocateDirect(64).order(ByteOrder.nativeOrder()).asIntBuffer();
		int[] viewport = new int[4];

		int hits;
		GL11.glGetInteger(GL11.GL_VIEWPORT, viewBuffer);
		viewBuffer.get(viewport);

		GL11.glSelectBuffer(selBuffer);
		GL11.glRenderMode(GL11.GL_SELECT);
		GL11.glInitNames();
		GL11.glPushName(0);
		GL11.glPushMatrix();
		{
			//change mode to selection
			GL11.glMatrixMode(GL11.GL_PROJECTION); // Select The Projection Matrix
			GL11.glLoadIdentity(); // Reset The Projection Matrix
			GLU.gluPickMatrix(x, y, 1, 1, IntBuffer.wrap(viewport));
			GLU.gluPerspective(45.0f, ((float) WIDTH / (float) HEIGHT), 0.1f, 100.0f); // Calculate The Aspect Ratio Of The Window

			GL11.glMatrixMode(GL11.GL_MODELVIEW); // Select The Modelview Matrix
			GL11.glLoadIdentity(); // Reset The Modelview Matrix

			renderGL();

			//reset
			GL11.glMatrixMode(GL11.GL_PROJECTION); // Select The Projection Matrix
			GL11.glLoadIdentity(); // Reset The Projection Matrix
			GLU.gluPerspective(45.0f, ((float) WIDTH / (float) HEIGHT), 0.1f, 100.0f); // Calculate The Aspect Ratio Of The Window

			GL11.glMatrixMode(GL11.GL_MODELVIEW); // Select The Modelview Matrix
			GL11.glLoadIdentity(); // Reset The Modelview Matrix
		}
		GL11.glPopMatrix();
		hits = GL11.glRenderMode(GL11.GL_RENDER);

		selBuffer.get(buffer);
		if (hits > 0)
		{
			int choose = buffer[3];
			int depth = buffer[1];

			for (int i = 1; i < hits; i++)
			{
				if ((buffer[i * 4 + 1] < depth || choose == 0) && buffer[i * 4 + 3] != 0)
				{
					choose = buffer[i * 4 + 3];
					depth = buffer[i * 4 + 1];
				}
			}

			if (choose > 0)
			{
				return choose - 1;
			}
		}

		return -1;
	}

	public void keyPressed(int key) {
		//System.out.println(key+" pressed!");
	}

	public void keyReleased(int key) {
		//System.out.println(key+" released!");
		if(key==Keyboard.KEY_RETURN) {
			wireframe = !wireframe;
		} else if(key==Keyboard.KEY_F) {
			if(newModel!=null) {
				if(newModel.getSelection()!=null) {
					JDialog dialog = new JDialog();
					dialog.setTitle("Model Part Info");
					JTextArea text = new JTextArea();
					text.setRows(3);
					text.setColumns(20);
					text.setText("IDs of this Model Part: \n\t Frame: "+newModel.getSelection().getFrame()+" \n\t Geometry: "+newModel.getSelection().getGeometry());
					text.setEditable(false);
					dialog.add(text);
					dialog.pack();
					dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
					dialog.setLocationRelativeTo(Display.getParent());
					dialog.setVisible(true);
					dialog.requestFocus();
				}
			}
		}
	}

	public void updateFPS() {
		if (getTime() - lastFPS > 1000) {
			//System.out.println("FPS: " + fps); 
			fps = 0; //reset the FPS counter
			lastFPS += 1000; //add one second
		}
		fps++;
	}

	public int getDelta() {
		long time = getTime();
		int delta = (int) (time - lastFrame);
		lastFrame = time;

		return delta;
	}

	public long getTime() {
		return System.nanoTime() / 1000000;
	}

	/**
	 * Set the display mode to be used 
	 * 
	 * @param width The width of the display required
	 * @param height The height of the display required
	 * @param fullscreen True if we want fullscreen mode
	 */
	public void setDisplayMode(int width, int height, boolean fullscreen) {
		// return if requested DisplayMode is already set
		if ((Display.getDisplayMode().getWidth() == width) && 
				(Display.getDisplayMode().getHeight() == height) && 
				(Display.isFullscreen() == fullscreen)) {
			return;
		}

		try {
			DisplayMode targetDisplayMode = null;

			if (fullscreen) {
				DisplayMode[] modes = Display.getAvailableDisplayModes();
				int freq = 0;

				for (int i=0;i<modes.length;i++) {
					DisplayMode current = modes[i];

					if ((current.getWidth() == width) && (current.getHeight() == height)) {
						if ((targetDisplayMode == null) || (current.getFrequency() >= freq)) {
							if ((targetDisplayMode == null) || (current.getBitsPerPixel() > targetDisplayMode.getBitsPerPixel())) {
								targetDisplayMode = current;
								freq = targetDisplayMode.getFrequency();
							}
						}

						// if we've found a match for bpp and frequence against the 
						// original display mode then it's probably best to go for this one
						// since it's most likely compatible with the monitor
						if ((current.getBitsPerPixel() == Display.getDesktopDisplayMode().getBitsPerPixel()) &&
								(current.getFrequency() == Display.getDesktopDisplayMode().getFrequency())) {
							targetDisplayMode = current;
							break;
						}
					}
				}
			} else {
				targetDisplayMode = new DisplayMode(width,height);
			}

			if (targetDisplayMode == null) {
				System.out.println("Failed to find value mode: "+width+"x"+height+" fs="+fullscreen);
				return;
			}

			Display.setDisplayMode(targetDisplayMode);
			Display.setFullscreen(fullscreen);

		} catch (LWJGLException e) {
			System.out.println("Unable to setup mode "+width+"x"+height+" fullscreen="+fullscreen + e);
		}
	}

	public boolean isRunning() {
		return running;
	}
}
