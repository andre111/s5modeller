package me.andre111.s5modeller.gui;

import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

public class FileGui {
	private static String title;
	private static String startPath;
	private static String[] endings;
	private static String fileTypeDesc;
	
	private static File file;
	
	public static void setTitle(String t) {
		title = t;
	}
	public static void setStartPath(String s) {
		startPath = s;
	}
	public static void setAcceptedFiles(String desc, String... e) {
		fileTypeDesc = desc;
		endings = e;
	}

	public static boolean showSaveDialog(S5MGui gui) {
		return showSaveDialog(gui, false);
	}
	public static boolean showSaveDialog(S5MGui gui, boolean chooseDir) {
		JFileChooser fc = createChooser();
		if(chooseDir) {
			fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		}
		
		if(fc.showSaveDialog(gui)==JFileChooser.APPROVE_OPTION) {
			file = fc.getSelectedFile();
			if(chooseDir==file.isDirectory()) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean showOpenDialog(S5MGui gui) {
		return showOpenDialog(gui, false);
	}
	public static boolean showOpenDialog(S5MGui gui, boolean chooseDir) {
		JFileChooser fc = createChooser();
		if(chooseDir) {
			fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		}
		
		if(fc.showOpenDialog(gui)==JFileChooser.APPROVE_OPTION) {
			file = fc.getSelectedFile();
			if(chooseDir==file.isDirectory()) {
				return true;
			}
		}
		return false;
	}
	
	private static JFileChooser createChooser() {
		JFileChooser fc = new JFileChooser();
		fc.setDialogTitle(title);
		File mdir = new File(startPath);
		if(!mdir.exists()) mdir.mkdirs();
		if(mdir.exists()) {
			fc.setCurrentDirectory(mdir);
		}
		fc.setAcceptAllFileFilterUsed(false);
		fc.addChoosableFileFilter(new FileFilter(){
			@Override
			public boolean accept(File f) {
				if(f.isDirectory()) return true;
				for(String ending : endings) {
					if(f.getName().toLowerCase().endsWith(ending)) return true;
				}
				
				return false;
			}

			@Override
			public String getDescription() {
				return fileTypeDesc;
			}
		});
		return fc;
	}
	
	public static File getFile() {
		return file;
	}
}
