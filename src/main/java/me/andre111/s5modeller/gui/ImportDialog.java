package me.andre111.s5modeller.gui;

import java.awt.GridLayout;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import me.andre111.s5modeller.rwbs.RWFromObj;
import me.andre111.s5modeller.rwbs.RWSection;
import me.andre111.s5modeller.rwbs.data.RWInteger;
import me.andre111.s5modeller.rwbs.data.RWShort;
import me.andre111.s5modeller.rwbs.data.RWString;
import me.andre111.s5modeller.rwbs.util.RWHacks;
import me.andre111.s5modeller.rwbs.util.RWUtil;

public class ImportDialog extends JDialog {
	private static final long serialVersionUID = 1L;

	private S5MGui gui;

	public ImportDialog(S5MGui gui) {
		super(gui, "Import .obj Model", true);

		this.gui = gui;

		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(13, 3));

		//separator
		panel.add(new Label("Model Name:"));
		JTextField nameField = new JTextField();
		panel.add(nameField);
		panel.add(new Label(""));

		//separator
		panel.add(new Label());
		panel.add(new Label());
		panel.add(new Label());

		//model file selection
		panel.add(new Label("Model File: "));
		JTextField modelField = new JTextField();
		modelField.setEditable(false);
		panel.add(modelField);
		JButton modelButton = new JButton("Choose File");
		panel.add(modelButton);

		//texture file selection
		panel.add(new Label("Texture File: "));
		JTextField textureField = new JTextField();
		textureField.setEditable(false);
		panel.add(textureField);
		JButton textureButton = new JButton("Choose File");
		panel.add(textureButton);

		//separator
		panel.add(new Label());
		panel.add(new Label());
		panel.add(new Label());

		panel.add(new Label("Effect: "));
		panel.add(new Label());
		panel.add(new Label());

		//effect selection
		JRadioButton noEffect = new JRadioButton("None");
		panel.add(noEffect);
		panel.add(new Label());
		panel.add(new Label());
		
		JRadioButton snowEffect = new JRadioButton("Snow");
		panel.add(snowEffect);
		JRadioButton decalEffect = new JRadioButton("Decal");
		panel.add(decalEffect);
		JRadioButton snowDecalEffect = new JRadioButton("Decal+Snow");
		panel.add(snowDecalEffect);
		
		JRadioButton specularEffect = new JRadioButton("Specular");
		panel.add(specularEffect);
		JRadioButton playerEffect = new JRadioButton("Playercolor");
		panel.add(playerEffect);
		JRadioButton specularPlayerEffect = new JRadioButton("Playercolor+Specular");
		panel.add(specularPlayerEffect);
		
		ButtonGroup group = new ButtonGroup();
		group.add(noEffect);
		group.add(snowEffect);
		group.add(decalEffect);
		group.add(snowDecalEffect);
		group.add(specularEffect);
		group.add(playerEffect);
		group.add(specularPlayerEffect);

		//Second model used for uvs
		panel.add(new Label("UV Model File: "));
		JTextField uvmodelField = new JTextField();
		uvmodelField.setEditable(false);
		panel.add(uvmodelField);
		JButton uvmodelButton = new JButton("Choose File");
		uvmodelButton.addActionListener((ActionEvent e) -> {

		});
		panel.add(uvmodelButton);

		//texture for use in effect
		panel.add(new Label("Effect Texture File: "));
		JTextField texture2Field = new JTextField();
		texture2Field.setEditable(false);
		panel.add(texture2Field);
		JButton texture2Button = new JButton("Choose File");
		texture2Button.addActionListener((ActionEvent e) -> {

		});
		panel.add(texture2Button);

		panel.add(new Label());
		panel.add(new Label());
		panel.add(new Label());

		panel.add(new Label());
		JButton cancelButton = new JButton("Cancel");
		panel.add(cancelButton);
		JButton okButton = new JButton("OK");
		panel.add(okButton);

		//action listeners
		modelButton.addActionListener((ActionEvent e) -> {
			FileGui.setTitle("Select Model");
			FileGui.setStartPath("./");
			FileGui.setAcceptedFiles(".obj Files", ".obj");
			if(FileGui.showOpenDialog(gui)) {
				File modelFile = FileGui.getFile();
				modelField.setText(modelFile.getAbsolutePath());

				//automatically set texture file
				if(textureField.getText()==null || textureField.getText().isEmpty()) {
					for(String ending : new String[] {".png", ".dds"}) {
						File textureFile = new File(modelFile.getParentFile(), modelFile.getName().substring(0, modelFile.getName().lastIndexOf("."))+ending);
						if(textureFile.exists()) {
							textureField.setText(textureFile.getAbsolutePath());
						}
					}
				}
			}
		});
		textureButton.addActionListener((ActionEvent e) -> {
			FileGui.setTitle("Select Texture");
			FileGui.setStartPath("./");
			FileGui.setAcceptedFiles("Texture Files", ".png", ".dds");
			if(FileGui.showOpenDialog(gui)) {
				File textureFile = FileGui.getFile();
				textureField.setText(textureFile.getAbsolutePath());
			}
		});

		uvmodelButton.addActionListener((ActionEvent e) -> {
			FileGui.setTitle("Select Model");
			FileGui.setStartPath("./");
			FileGui.setAcceptedFiles(".obj Files", ".obj");
			if(FileGui.showOpenDialog(gui)) {
				File uvmodelFile = FileGui.getFile();
				uvmodelField.setText(uvmodelFile.getAbsolutePath());

				//automatically set texture file
				if(texture2Field.getText()==null || texture2Field.getText().isEmpty()) {
					for(String ending : new String[] {".png", ".dds"}) {
						File texture2File = new File(uvmodelFile.getParentFile(), uvmodelFile.getName().substring(0, uvmodelFile.getName().lastIndexOf("."))+ending);
						if(texture2File.exists()) {
							texture2Field.setText(texture2File.getAbsolutePath());
						}
					}
				}
			}
		});
		texture2Button.addActionListener((ActionEvent e) -> {
			FileGui.setTitle("Select Texture");
			FileGui.setStartPath("./");
			FileGui.setAcceptedFiles("Texture Files", ".png", ".dds");
			if(FileGui.showOpenDialog(gui)) {
				File texture2File = FileGui.getFile();
				texture2Field.setText(texture2File.getAbsolutePath());
			}
		});
		
		ActionListener enableTexture2 = (ActionEvent e) -> {
			uvmodelField.setEnabled(true);
			uvmodelButton.setEnabled(true);
			texture2Field.setEnabled(true);
			texture2Button.setEnabled(true);
		};
		ActionListener disableTexture2 = (ActionEvent e) -> {
			uvmodelField.setEnabled(false);
			uvmodelButton.setEnabled(false);
			texture2Field.setEnabled(false);
			texture2Button.setEnabled(false);
		};

		noEffect.setSelected(true);
		disableTexture2.actionPerformed(null);
		noEffect.addActionListener(disableTexture2);
		snowEffect.addActionListener(enableTexture2);
		decalEffect.addActionListener(disableTexture2);
		snowDecalEffect.addActionListener(enableTexture2);
		specularEffect.addActionListener(enableTexture2);
		playerEffect.addActionListener(disableTexture2);
		specularPlayerEffect.addActionListener(enableTexture2);

		cancelButton.addActionListener((ActionEvent e) -> this.setVisible(false));
		okButton.addActionListener((ActionEvent e) -> {
			if(nameField.getText()==null || nameField.getText().isEmpty()) {
				JOptionPane.showMessageDialog(this, "Please enter a name!");
				return;
			}
			
			File modelFile = new File(modelField.getText());
			File textureFile = new File(textureField.getText());

			if(!modelFile.exists()) {
				JOptionPane.showMessageDialog(this, "Model file does not exist!");
				return;
			}
			if(!textureFile.exists()) {
				JOptionPane.showMessageDialog(this, "Texture file does not exist!");
				return;
			}

			String effect = "none";
			File uvmodelFile = null;
			File texture2File = null;
			if(snowEffect.isSelected()) effect = "snow";
			if(decalEffect.isSelected()) effect = "decal";
			if(snowDecalEffect.isSelected()) effect = "snowDecal";
			if(specularEffect.isSelected()) effect = "spec";
			if(playerEffect.isSelected()) effect = "player";
			if(specularPlayerEffect.isSelected()) effect = "specPlayer";

			if(effect.equals("snow") || effect.equals("snowDecal") || effect.equals("spec") || effect.equals("specPlayer")) {
				uvmodelFile = new File(uvmodelField.getText());
				texture2File = new File(texture2Field.getText());

				if(!uvmodelFile.exists()) {
					JOptionPane.showMessageDialog(this, "UVModel file does not exist!");
					return;
				}
				if(!texture2File.exists()) {
					JOptionPane.showMessageDialog(this, "Effect Texture file does not exist!");
					return;
				}
			}

			this.setEnabled(false);
			if(importObj(nameField.getText(), modelFile, textureFile, effect, uvmodelFile, texture2File)) {
				this.dispose();
			} else {
				this.setEnabled(true);
			}
		});

		this.add(panel);

		this.pack();
		this.setLocationRelativeTo(gui);
		this.setVisible(true);
	}

	private boolean importObj(String modelName, File modelFile, File textureFile, String effect, File uvmodelFile, File texture2File) {
		//this should never happen, as it is already checked before, but just in case...
		if(modelName==null || modelName.isEmpty()) return false;
		if(modelFile==null) return false;
		if(textureFile==null) return false;
		if((effect.equals("snow") || effect.equals("snowDecal") || effect.equals("spec") || effect.equals("specPlayer")) && uvmodelFile==null) return false;
		if((effect.equals("snow") || effect.equals("snowDecal") || effect.equals("spec") || effect.equals("specPlayer")) && texture2File==null) return false;

		//convert textures if need be
		textureFile = convertTextureIfNeeded(textureFile, "./data/tmp1.dds");
		if(textureFile==null) return false;
		if(!effect.equals("none")) {
			texture2File = convertTextureIfNeeded(texture2File, "./data/tmp2.dds");
			if(texture2File==null) return false;
		}

		//TODO: model/texture name should probably get set in the gui
		String textureName = modelName;
		String texture2Name = textureName+"_"+effect;

		//convert model
		int version = 469893130; //TODO: These should be located somewhere globally
		RWSection modelSection = RWFromObj.convertOBJ(modelFile, textureName);
		if(modelSection==null) {
			JOptionPane.showMessageDialog(this, "Error importing Model!");
			return false;
		}

		//add texture effect(convert second model)
		if(!effect.equals("none")) {
			//copy uvs
			if(effect.equals("snow") || effect.equals("snowDecal") || effect.equals("spec") || effect.equals("specPlayer")) {
				RWSection uvModelSection = RWFromObj.convertOBJ(uvmodelFile, texture2Name);
				if(uvModelSection==null) {
					JOptionPane.showMessageDialog(this, "Error importing UVModel!");
					return false;
				}
				
				RWHacks.copyUVasUV2(modelSection, uvModelSection);
			}

			//TODO: add required sections/make required changes
			if(effect.equals("snow")) {
				//userdata string in frame extension
				addUserDataPLG(version, modelSection, "Effect=SimpleObjectWithSnow");
				
				//MaterialEffectsPLG with reference to texture
				addMaterialEffectsPLG(version, modelSection, 5, 6, texture2Name);
			} else if(effect.equals("decal")) {
				//userdata string in frame extension
				addUserDataPLG(version, modelSection, "decal=doodad");
			} else if(effect.equals("snowDecal")) {
				//userdata string in frame extension
				addUserDataPLG(version, modelSection, "decal=flat", "Effect=BuildingDecalWithSnow");

				//MaterialEffectsPLG with reference to texture
				addMaterialEffectsPLG(version, modelSection, 5, 6, texture2Name);
			} else if(effect.equals("spec")) {
				//MaterialEffectsPLG with reference to texture
				addMaterialEffectsPLG(version, modelSection, 2, 2, texture2Name);
			} else if(effect.equals("player")) {
				//userdata string in frame extension
				addUserDataPLG(version, modelSection, "Effect=SimpleObjectPlayerColor");
			} else if(effect.equals("specPlayer")) {
				//TODO: Soldiers dont appear to have a "playercolor" userdata, is models.xml used here?
				
				//MaterialEffectsPLG with reference to texture
				addMaterialEffectsPLG(version, modelSection, 2, 2, texture2Name);
			}
		}

		//TODO: save files (section/dff + textures)
		//write model
		File target = new File("./data/out/graphics/models/"+modelName+".dff");
		if(!target.getParentFile().exists()) {
			target.getParentFile().mkdirs();
		}
		if(target.exists()) {
			if(JOptionPane.showConfirmDialog(this, "Model allready exists in /data/out/... Overwrite?")!=JOptionPane.OK_OPTION) {
				return false;
			}
		}
		if(!RWUtil.saveRWSection(modelSection, target)) {
			JOptionPane.showMessageDialog(this, "Could not save model!");
			return false;
		}
		//save textures
		for(String folder : new String[]{"textures", "textureslow", "texturesmed"}) {
			File ddsFile = new File("./data/out/graphics/"+folder+"/"+textureName+".dds");
			if(!ddsFile.getParentFile().exists()) {
				ddsFile.getParentFile().mkdirs();
			}
			try {
				Files.copy(textureFile.toPath(), ddsFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
			} catch (IOException e) {
				JOptionPane.showMessageDialog(this, "Could not save texture!");
				e.printStackTrace();
				return false;
			}

			if(effect.equals("snow") || effect.equals("snowDecal") || effect.equals("spec") || effect.equals("specPlayer")) {
				ddsFile = new File("./data/out/graphics/"+folder+"/"+texture2Name+".dds");
				if(!ddsFile.getParentFile().exists()) {
					ddsFile.getParentFile().mkdirs();
				}
				try {
					Files.copy(texture2File.toPath(), ddsFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
				} catch (IOException e) {
					JOptionPane.showMessageDialog(this, "Could not save texture!");
					e.printStackTrace();
					return false;
				}
			}
		}

		gui.update(modelSection);

		return true;
	}

	private File convertTextureIfNeeded(File texture, String tmpPath) {
		if(texture!=null && !texture.getName().substring(texture.getName().lastIndexOf(".")).equals(".dds")) {
			File targetFile = new File(tmpPath);

			//check for ImageMagick
			if(!new File("./ImageMagick/magick.exe").exists()) {
				JOptionPane.showMessageDialog(this, "Texture needs to be converted but ImageMagick was not found!");
				return null;
			}

			try {
				Process process = new ProcessBuilder("./ImageMagick/magick.exe", 
						texture.getPath(), 
						"-define", "dds:compression=dxt5",
						"-define", "dds:mipmaps=0",
						targetFile.getPath()).start();

				process.waitFor();
			} catch (IOException | InterruptedException e) {
				JOptionPane.showMessageDialog(this, "Error converting texture!");
				e.printStackTrace();
				return null;
			}

			return targetFile;
		} else {
			return texture;
		}
	}
	
	private void addUserDataPLG(int version, RWSection root, String... strings) {
		RWSection frameList = RWUtil.getSection(root, 0x000E);
		//TODO: Handle multiple frames (importer does not create multiple currently, so low priority)
		RWSection frameExt = RWUtil.getSection(frameList, 0x0003, 1);
		
		RWSection userdataplg = new RWSection(frameExt, 0x011F, version);
		{
			userdataplg.addData("unknown", new RWInteger().setInteger(1));
			userdataplg.addData("titleLength", new RWInteger().setInteger(23));
			userdataplg.addData("title", new RWString("3dsmax User Properties"));
			userdataplg.addData("type", new RWInteger().setInteger(3));
			userdataplg.addData("count", new RWInteger().setInteger(strings.length));
			for(String st : strings) {
				userdataplg.addData("strSize", new RWInteger().setInteger(st.length()+1));
				userdataplg.addData("title", new RWString(st));
			}
		}
		frameExt.addData("", userdataplg);
	}

	private void addMaterialEffectsPLG(int version, RWSection root, int srcBlend, int dstBlend, String texName) {
		//Find Material Extension
		RWSection geometryList = RWUtil.getSection(root, 0x001A);

		int g = 0;
		while(RWUtil.getSection(geometryList, 0x000F, g)!=null) {
			RWSection materialList = RWUtil.getSection(RWUtil.getSection(geometryList, 0x000F, g), 0x0008);
			//TODO: Handle multiple materials (importer does not create multiple currently, so low priority)
			RWSection material = RWUtil.getSection(materialList, 0x0007); 
			RWSection matExt = RWUtil.getSection(material, 0x0003);

			RWSection mateffplg = new RWSection(matExt, 0x0120, version);
			mateffplg.addData("type", new RWInteger().setInteger(0x00000004));
			mateffplg.addData("type1", new RWInteger().setInteger(0x00000004));
			mateffplg.addData("srcBlend", new RWInteger().setInteger(srcBlend));
			mateffplg.addData("dstBlend", new RWInteger().setInteger(dstBlend));
			mateffplg.addData("hasDualPassMap", new RWInteger().setInteger(0x00000001));
			{
				RWSection tex = new RWSection(mateffplg, 0x0006, version);
				{
					RWSection texStruct = new RWSection(tex, 0x0001, version);
					{
						texStruct.addData("flags", new RWShort().setShort((short) 0x1106));
						texStruct.addData("unknown", new RWShort().setShort((short) 1));
					}
					tex.addData("", texStruct);

					RWSection diffuse = new RWSection(tex, 0x0002, version);
					diffuse.addData("string", new RWString(texName));
					tex.addData("", diffuse);

					RWSection mask = new RWSection(tex, 0x0002, version);
					mask.addData("string", new RWString(""));
					tex.addData("", mask);

					tex.addData("", new RWSection(tex, 0x0003, version));
				}
				mateffplg.addData("", tex);
			}
			mateffplg.addData("unknown", new RWInteger().setInteger(0));
			matExt.addData("", mateffplg);

			g++;
		}
	}
}
