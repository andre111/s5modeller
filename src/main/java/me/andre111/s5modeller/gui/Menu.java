package me.andre111.s5modeller.gui;

import java.awt.event.ActionEvent;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class Menu extends JMenuBar {
	private static final long serialVersionUID = 7013334224439130307L;
	
	public S5MGui gui;
	
	public Menu(S5MGui gui) {
		this.gui = gui;
		
		JMenu file = new JMenu("File");
		{
			JMenuItem open = new JMenuItem("Open");
			open.addActionListener((ActionEvent e) -> openFile());
			file.add(open);
			
			//TODO: Save
			JMenuItem save = new JMenuItem("Save");
			save.addActionListener((ActionEvent e) -> saveFile());
			file.add(save);
			
			file.addSeparator();
			
			JMenuItem imp = new JMenuItem("Import .obj");
			imp.addActionListener((ActionEvent e) -> importObj());
			file.add(imp);
			
			JMenuItem exp = new JMenuItem("Export .obj");
			exp.addActionListener((ActionEvent e) -> exportObj());
			file.add(exp);
			
			file.addSeparator();
			
			JMenuItem batch = new JMenuItem("Batch Process");
			batch.addActionListener((ActionEvent e) -> batchProcess());
			file.add(batch);
		}
		add(file);
		
		JMenu model = new JMenu("Model");
		{
			JMenuItem display = new JMenuItem("Display Current");
			display.addActionListener((ActionEvent e) -> gui.displayCurrent());
			model.add(display);
			JMenuItem transform = new JMenuItem("Transform Current");
			transform.addActionListener((ActionEvent e) -> gui.transformCurrent());
			model.add(transform);
		}
		add(model);
		
		JMenu animation = new JMenu("Animation");
		{
			JMenuItem skeleton = new JMenuItem("Extract Skeleton");
			skeleton.addActionListener((ActionEvent e) -> gui.extractSkeleton());
			animation.add(skeleton);
			
			JMenuItem loadAnimation = new JMenuItem("WIP: Load Animation");
			loadAnimation.addActionListener((ActionEvent e) -> gui.loadAnimation());
			animation.add(loadAnimation);
		}
		add(animation);
		
		JMenu hacks = new JMenu("Hacks");
		{
			JMenuItem changePos = new JMenuItem("Change Pos Of Frame");
			changePos.addActionListener((ActionEvent e) -> gui.doHack(0));
			hacks.add(changePos);
			
			JMenuItem changeRot = new JMenuItem("Change Rot Of Frame");
			changeRot.addActionListener((ActionEvent e) -> gui.doHack(1));
			hacks.add(changeRot);
			
			JMenuItem scale = new JMenuItem("Scale Frame");
			scale.addActionListener((ActionEvent e) -> gui.doHack(2));
			hacks.add(scale);
			
			JMenuItem destMod = new JMenuItem("Real hack: Destroy model");
			destMod.addActionListener((ActionEvent e) -> gui.doHack(3));
			hacks.add(destMod);
			
			JMenuItem addMod = new JMenuItem("Add Model");
			addMod.addActionListener((ActionEvent e) -> gui.doHack(4));
			hacks.add(addMod);
			
			JMenuItem copyUV = new JMenuItem("Real Hack: Copy UVs as UV2");
			copyUV.addActionListener((ActionEvent e) -> gui.doHack(5));
			hacks.add(copyUV);
			
			JMenuItem zeroUV = new JMenuItem("Real Hack: Zero Everything");
			zeroUV.addActionListener((ActionEvent e) -> gui.doHack(6));
			hacks.add(zeroUV);
			
			JMenuItem test = new JMenuItem("WIP - Testing Code");
			test.addActionListener((ActionEvent e) -> gui.doHack(7));
			hacks.add(test);
		}
		add(hacks);
	}
	
	public void openFile() {
		FileGui.setTitle("Select File");
		FileGui.setStartPath("./data/graphics/models/");
		FileGui.setAcceptedFiles("Renderware Files", ".dff", ".anm");
		if(FileGui.showOpenDialog(gui)) {
			System.out.println(FileGui.getFile());
			gui.openFile(FileGui.getFile());
		}
	}
	
	public void saveFile() {
		FileGui.setTitle("Select File");
		FileGui.setStartPath("./data/out/graphics/models/");
		FileGui.setAcceptedFiles("Renderware Files", ".dff", ".anm");
		if(FileGui.showSaveDialog(gui)) {
			System.out.println(FileGui.getFile());
			gui.saveFile(FileGui.getFile());
		}
	}
	
	public void importObj() {
		/*FileGui.setTitle("Select Model");
		FileGui.setStartPath("./");
		FileGui.setAcceptedFiles(".obj Files", ".obj");
		if(FileGui.showOpenDialog(gui)) {
			System.out.println(FileGui.getFile());
			gui.importObj(FileGui.getFile());
		}*/
		//gui.importObj(null);
		new ImportDialog(gui);
	}
	
	public void exportObj() {
		FileGui.setTitle("Select Directory");
		FileGui.setStartPath("./");
		FileGui.setAcceptedFiles("*");
		if(FileGui.showSaveDialog(gui, true)) {
			System.out.println(FileGui.getFile());
			gui.exportObj(FileGui.getFile());
		}
	}
	
	public void batchProcess() {
		FileGui.setTitle("Select Batch Description file");
		FileGui.setStartPath("./");
		FileGui.setAcceptedFiles(".txt Files", ".txt");
		if(FileGui.showOpenDialog(gui)) {
			System.out.println(FileGui.getFile());
			gui.doBatchProcess(FileGui.getFile());
		}
	}
}
