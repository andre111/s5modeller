package me.andre111.s5modeller.gui;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Enumeration;
import java.util.Iterator;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import me.andre111.s5modeller.display.S5MDisplay;
import me.andre111.s5modeller.model.Animation;
import me.andre111.s5modeller.model.Model;
import me.andre111.s5modeller.model.ModelPart;
import me.andre111.s5modeller.model.Skeleton;
import me.andre111.s5modeller.rwbs.ObjFromRW;
import me.andre111.s5modeller.rwbs.RWInfo;
import me.andre111.s5modeller.rwbs.RWSection;
import me.andre111.s5modeller.rwbs.data.RWFloat;
import me.andre111.s5modeller.rwbs.data.RWFloatArray2D;
import me.andre111.s5modeller.rwbs.data.RWInteger;
import me.andre111.s5modeller.rwbs.data.RWString;
import me.andre111.s5modeller.rwbs.util.RWHacks;
import me.andre111.s5modeller.rwbs.util.RWUtil;
import me.andre111.s5modeller.util.CombinedModel;
import me.andre111.s5modeller.util.EndianCorrectOutputStream;
import me.andre111.s5modeller.util.MatUtil;

public class S5MGui extends JFrame implements WindowListener, TreeSelectionListener, MouseListener {
	private static final long serialVersionUID = 877509639750877001L;

	private JTree tree;
	private DefaultMutableTreeNode top;

	private Menu menu;
	private SectionGui secGui;

	private S5MDisplay display;

	public S5MGui() {
		menu = new Menu(this);
		this.setJMenuBar(menu);

		JPanel container = new JPanel();
		container.setLayout(new BoxLayout(container, BoxLayout.X_AXIS));
		{
			JScrollPane scroll = new JScrollPane();
			scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
			scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			scroll.setPreferredSize(new Dimension(600, 800));
			scroll.setMinimumSize(new Dimension(500, 20));
			{
				top = new DefaultMutableTreeNode("");
				tree = new JTree(top);	
				tree.addTreeSelectionListener(this);
				tree.addMouseListener(this);
				scroll.getViewport().add(tree);
			}
			container.add(scroll);

			secGui = new SectionGui(this);
			container.add(secGui);
		}
		this.add(container);

		this.pack();
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.addWindowListener(this);

		//openFile(new File("./data/graphics/models/pu_hero5.dff"));
	}

	@Override
	public void windowClosing(WindowEvent e) {
		if(display!=null) {
			display.stop();
		}
		this.dispose();
	}

	@Override
	public void valueChanged(TreeSelectionEvent e) {
		if(e.isAddedPath()) {
			DefaultMutableTreeNode node = (DefaultMutableTreeNode) e.getPath().getLastPathComponent();
			if(node.getUserObject() instanceof RWSection) {
				secGui.setSection((RWSection) node.getUserObject());
			}
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		if (SwingUtilities.isRightMouseButton(e)) {
			int row = tree.getClosestRowForLocation(e.getX(), e.getY());
			tree.setSelectionRow(row);

			JPopupMenu popupMenu = new JPopupMenu();

			JMenuItem deleteSection = new JMenuItem("Delete Section");
			deleteSection.addActionListener((ActionEvent ae) -> deleteSelectedSection());
			popupMenu.add(deleteSection);

			popupMenu.addSeparator();

			JMenuItem exportSection = new JMenuItem("Export Section Data");
			exportSection.addActionListener((ActionEvent ae) -> saveSelectedSection());
			popupMenu.add(exportSection);
			JMenuItem importSection = new JMenuItem("Import Section Data");
			importSection.addActionListener((ActionEvent ae) -> loadSelectedSection());
			popupMenu.add(importSection);
			JMenuItem addSection = new JMenuItem("Add Section");
			addSection.addActionListener((ActionEvent ae) -> addSection());
			popupMenu.add(addSection);

			popupMenu.show(e.getComponent(), e.getX(), e.getY());
		}
	}

	public void openFile(File file) {
		update(RWUtil.loadRWSection(file));
	}

	public void saveFile(File file) {
		RWSection sec = getRootSection();
		if(sec==null) return;

		RWUtil.saveRWSection(sec, file);
	}

	//TODO: After the export:
	//			Import the Model using Blender
	//			Scale it to 0.01 in all dimensions
	//			Rotate it by -90 on x Axis
	//			... have fun editing
	//          (might need "Make Normals consistent" in edit mode - maybe not anymore using the new exporter?)
	//
	//			Small tip: In editmode with all vertices selected press P and select 
	//                     "Separate -> By loose Parts" to split models into their parts
	//                     This way you can easily get weapons as their own single object
	//                     (Use "Set Origin" -> Geometry to Origin to set a better origin for the parts)
	public void exportObj(File folder) {

		Model model = new Model(getRootSection());
		int c = 0;
		for(ModelPart part : model.parts) {
			c++;
			File file = new File(folder, "part"+c+".obj");
			try(BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)))) {
				writer.write("mtllib part"+c+".mtl");
				writer.newLine();
				writer.write("usemtl part"+c);
				writer.newLine();
				writer.write("s 1");
				writer.newLine();
				ObjFromRW.convertRW(part, writer);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			//write mtllib
			File mtllib = new File(folder, "part"+c+".mtl");
			try(BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(mtllib)))) {
				writer.write("newmtl part"+c);
				writer.newLine();
				writer.write("Ks 0.2 0.2 0.2");
				writer.newLine();
				writer.write("map_Kd part"+c+".dds");
				writer.newLine();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			//copy texture
			if(part.getTextureName()!=null) { //because some (broken) models dont have a texture definition
				String texFile = "./data/graphics/textures/"+part.getTextureName().toLowerCase()+".dds";
				if(!new File(texFile).exists()) texFile = "./data/out/graphics/textures/"+part.getTextureName().toLowerCase()+".dds";
	
				try {
					Files.copy(new File(texFile).toPath(), new File(folder, "part"+c+".dds").toPath(), StandardCopyOption.REPLACE_EXISTING);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	public void recreate() {
		//find selected section
		RWSection sec = null;
		if(tree.getSelectionModel().getSelectionPath()!=null) {
			DefaultMutableTreeNode node = (DefaultMutableTreeNode)tree.getSelectionModel().getSelectionPath().getLastPathComponent();
			if(node.getUserObject() instanceof RWSection) {
				sec = (RWSection) node.getUserObject();
			}
		}

		//TreePath sel = tree.getSelectionPath();
		update(getRootSection());
		//tree.setSelectionPath(sel);

		//try to recreate selection
		if(sec!=null) {
			DefaultMutableTreeNode nodeToSelect = null;
			DefaultMutableTreeNode root = (DefaultMutableTreeNode)tree.getModel().getRoot();
			@SuppressWarnings({ "rawtypes" })
			Enumeration e = root.preorderEnumeration();
			while(e.hasMoreElements()){
				DefaultMutableTreeNode cnode = (DefaultMutableTreeNode) e.nextElement();
				if(cnode.getUserObject() instanceof RWSection) {
					if(cnode.getUserObject()==sec) {
						nodeToSelect = cnode;
					}
				}
			}

			if(nodeToSelect!=null) {
				TreePath path = new TreePath(nodeToSelect.getPath());
				tree.getSelectionModel().setSelectionPath(path);
				tree.scrollPathToVisible(path);
			}
		}

		this.validate();
	}

	public void update(RWSection parent) {
		secGui.reset();
		updateTree(parent);
	}

	public void updateTree(RWSection parent) {
		DefaultMutableTreeNode node = new DefaultMutableTreeNode(parent);
		fillTree(parent, node);

		top.removeAllChildren();
		top.add(node);

		((DefaultTreeModel) tree.getModel()).reload();

		expandAllNodes(tree, 0, tree.getRowCount());
	}

	private void fillTree(RWSection parent, DefaultMutableTreeNode pnode) {
		for(RWInfo info : parent.getData()) {
			if(info.getData() instanceof RWSection) {
				RWSection child = (RWSection) info.getData();
				DefaultMutableTreeNode node = new DefaultMutableTreeNode(child);
				fillTree(child, node);
				pnode.add(node);
			}
		}
	}

	private void expandAllNodes(JTree tree, int startingIndex, int rowCount){
		for(int i=startingIndex; i<rowCount; ++i){
			tree.expandRow(i);
		}

		if(tree.getRowCount()!=rowCount){
			expandAllNodes(tree, rowCount, tree.getRowCount());
		}
	}

	public void displayCurrent() {
		RWSection sec = getRootSection();
		if(sec==null) return;

		if(display==null || !display.isRunning()) {
			display = new S5MDisplay(this);
			display.start();
		}

		display.setModel(sec);
	}

	public void transformCurrent() {
		RWSection sec = getRootSection();
		if(sec==null) return;

		JDialog dialog = new JDialog(this, "Transform Model", true);

		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(9, 2));

		JTextField rotateAngle = new JTextField("0");
		panel.add(new Label("Rotation Angle"));
		panel.add(rotateAngle);

		JComboBox<String> rotateAxis = new JComboBox<String>();
		rotateAxis.addItem("X");
		rotateAxis.addItem("Y");
		rotateAxis.addItem("Z");
		rotateAxis.setSelectedIndex(2);
		panel.add(new Label("Rotation Axis"));
		panel.add(rotateAxis);

		JTextField[] scale = {
				new JTextField("1"),	
				new JTextField("1"),
				new JTextField("1")
		};
		panel.add(new Label("Scale X"));
		panel.add(scale[0]);
		panel.add(new Label("Scale Y"));
		panel.add(scale[1]);
		panel.add(new Label("Scale Z"));
		panel.add(scale[2]);

		JTextField[] translate = {
				new JTextField("0"),	
				new JTextField("0"),
				new JTextField("0")
		};
		panel.add(new Label("Translate X"));
		panel.add(translate[0]);
		panel.add(new Label("Translate Y"));
		panel.add(translate[1]);
		panel.add(new Label("Translate Z"));
		panel.add(translate[2]);

		JButton cancelButton = new JButton("Cancel");
		panel.add(cancelButton);
		cancelButton.addActionListener((ActionEvent e) -> dialog.dispose());
		JButton okButton = new JButton("OK");
		panel.add(okButton);
		okButton.addActionListener((ActionEvent e) -> {
			try {
				float rotationAngle = (float) Math.toRadians(Float.parseFloat(rotateAngle.getText()));
				String rotationAxis = (String) rotateAxis.getSelectedItem();

				float scaleX = Float.parseFloat(scale[0].getText());
				float scaleY = Float.parseFloat(scale[1].getText());
				float scaleZ = Float.parseFloat(scale[2].getText());

				float translateX = Float.parseFloat(translate[0].getText());
				float translateY = Float.parseFloat(translate[1].getText());
				float translateZ = Float.parseFloat(translate[2].getText());

				modifyGeometry(rotationAngle, rotationAxis, scaleX, scaleY, scaleZ, translateX, translateY, translateZ);

				dialog.dispose();
			} catch(NumberFormatException ex) {
				JOptionPane.showMessageDialog(dialog, "Input is not a number!");
			}
		});

		dialog.add(panel);

		dialog.pack();
		dialog.setLocationRelativeTo(this);
		dialog.setVisible(true);
	}

	private void modifyGeometry(float rotationAngle, String rotationAxis, float scaleX, float scaleY, float scaleZ, float translateX, float translateY, float translateZ) {
		RWSection sec = getRootSection();
		if(sec==null) return;

		RWFloatArray2D rotMat = MatUtil.getRotMatrix(rotationAngle, rotationAxis);
		RWFloatArray2D rotMinusMat = MatUtil.getRotMatrix(-rotationAngle, rotationAxis);
		RWFloatArray2D scaleMat = MatUtil.getScaleMatrix(scaleX, scaleY, scaleZ);

		//loop through all atomics
		int pos = 0;
		while(RWUtil.getSection(sec, 0x0014, pos)!=null) {
			RWSection atomic = RWUtil.getSection(sec, 0x0014, pos);
			RWSection atomicStruct = RWUtil.getSection(atomic, 0x0001);
			int frame = ((RWInteger) RWUtil.getData(atomicStruct, "frameIndex")).getInteger();

			//find and copy corresponding framedata 
			//(TODO: frame may be in a tree structure (parentFrame) which has multiple layered translations)
			RWSection oldFrameList = RWUtil.getSection(sec, 0x000E);
			RWSection oldFrameListStruct = RWUtil.getSection(oldFrameList, 0x0001);

			RWFloatArray2D mat = (RWFloatArray2D) RWUtil.getData(oldFrameListStruct, "rotationMatrix", frame);
			MatUtil.setValues(mat, MatUtil.multiply(mat, rotMat));
			MatUtil.setValues(mat, MatUtil.multiply(mat, scaleMat));

			RWFloat offX = (RWFloat) RWUtil.getData(oldFrameListStruct, "coordinateOffsetX", frame);
			RWFloat offY = (RWFloat) RWUtil.getData(oldFrameListStruct, "coordinateOffsetY", frame);
			RWFloat offZ = (RWFloat) RWUtil.getData(oldFrameListStruct, "coordinateOffsetZ", frame);
			float[] off = {
					offX.getFloat(),
					offY.getFloat(),
					offZ.getFloat()
			};
			off = MatUtil.multiplyVector(rotMinusMat, off);
			off = MatUtil.multiplyVector(scaleMat, off);
			offX.setFloat(off[0]+translateX);
			offY.setFloat(off[1]+translateY);
			offZ.setFloat(off[2]+translateZ);

			pos++;
		}
	}

	private RWSection getRootSection() {
		if(top.getChildCount()==0) return null;
		DefaultMutableTreeNode root = (DefaultMutableTreeNode)top.getChildAt(0);
		if(!(root.getUserObject() instanceof RWSection)) return null;

		return (RWSection) root.getUserObject();
	}
	
	public void extractSkeleton() {
		RWSection sec = getRootSection();
		if(sec==null) return;

		//TODO: Just testing stuff
		Skeleton s = new Skeleton(sec);
		if(display!=null) {
			display.applySkeleton(s);
		}
	}
	
	public void loadAnimation() {
		//TODO: Just testing stuff
		FileGui.setTitle("Select File");
		FileGui.setStartPath("./data/graphics/animations/");
		FileGui.setAcceptedFiles("Renderware Files", ".dff", ".anm");
		if(FileGui.showOpenDialog(this)) {
			System.out.println(FileGui.getFile());
			File file = FileGui.getFile();
			
			int baseBoneID = -1;
			try{
				baseBoneID = Integer.parseInt(JOptionPane.showInputDialog(this, " Enter Base Bone ID (leave -1 for default bone) (Buildings in Settlers 5 have this id at the end of the filename) : ", "-1"));
			} catch(NumberFormatException e) {
			}
			
			Animation a = new Animation(RWUtil.loadRWSection(file), baseBoneID);
			if(display!=null) {
				display.applyAnimation(a);
			}
		}
	}

	public void deleteSelectedSection() {
		DefaultMutableTreeNode node = (DefaultMutableTreeNode)tree.getSelectionModel().getSelectionPath().getLastPathComponent();
		if(!(node.getUserObject() instanceof RWSection)) return;

		RWSection sec = (RWSection) node.getUserObject();
		sec.getParent().removeData(sec);
		recreate();
	}

	public void saveSelectedSection() {
		DefaultMutableTreeNode node = (DefaultMutableTreeNode)tree.getSelectionModel().getSelectionPath().getLastPathComponent();
		if(!(node.getUserObject() instanceof RWSection)) return;

		RWSection sec = (RWSection) node.getUserObject();

		FileGui.setTitle("Select File");
		FileGui.setStartPath("./data/out/hex/");
		FileGui.setAcceptedFiles("HEX files", ".hex");
		if(FileGui.showSaveDialog(this)) {
			RWUtil.saveRWSection(sec, FileGui.getFile());
		}
	}

	public void loadSelectedSection() {
		DefaultMutableTreeNode node = (DefaultMutableTreeNode)tree.getSelectionModel().getSelectionPath().getLastPathComponent();
		if(!(node.getUserObject() instanceof RWSection)) return;

		RWSection sec = (RWSection) node.getUserObject();

		FileGui.setTitle("Select File");
		FileGui.setStartPath("./data/out/hex/");
		FileGui.setAcceptedFiles("HEX files", ".hex");
		if(FileGui.showOpenDialog(this)) {
			RWUtil.loadRWSection(sec, FileGui.getFile());
			recreate();
		}
	}

	public void addSection() {
		DefaultMutableTreeNode node = (DefaultMutableTreeNode)tree.getSelectionModel().getSelectionPath().getLastPathComponent();
		if(!(node.getUserObject() instanceof RWSection)) return;

		RWSection parent = (RWSection) node.getUserObject();

		FileGui.setTitle("Select File");
		FileGui.setStartPath("./data/out/hex/");
		FileGui.setAcceptedFiles("HEX files", ".hex");
		if(FileGui.showOpenDialog(this)) {
			RWSection sec = new RWSection(parent);
			RWUtil.loadRWSection(sec, FileGui.getFile());
			parent.addData("", sec);
			recreate();
		}
	}

	public void doHack(int pos) {
		//change frame pos
		if(pos==0) {
			RWSection root = getRootSection();
			RWSection frameList = RWUtil.getSection(root, 0x000E);
			RWSection struct = RWUtil.getSection(frameList, 0x0001);

			int frame = -1;
			try{
				frame = Integer.parseInt(JOptionPane.showInputDialog(this, " Enter Frame: ", ""+0));
			} catch(NumberFormatException e) {
			}
			if(frame==-1) return;

			RWFloat x = (RWFloat) RWUtil.getData(struct, "coordinateOffsetX", frame);
			RWFloat y = (RWFloat) RWUtil.getData(struct, "coordinateOffsetY", frame);
			RWFloat z = (RWFloat) RWUtil.getData(struct, "coordinateOffsetZ", frame);

			try{
				x.setFloat(Float.parseFloat(JOptionPane.showInputDialog(this, " Enter X: ", ""+x.getFloat())));
				y.setFloat(Float.parseFloat(JOptionPane.showInputDialog(this, " Enter Y: ", ""+y.getFloat())));
				z.setFloat(Float.parseFloat(JOptionPane.showInputDialog(this, " Enter Z: ", ""+z.getFloat())));
			} catch(NumberFormatException e) {
			}
			//change frame rotation
		} else if(pos==1) {
			RWSection root = getRootSection();
			RWSection frameList = RWUtil.getSection(root, 0x000E);
			RWSection struct = RWUtil.getSection(frameList, 0x0001);

			int frame = -1;
			try{
				frame = Integer.parseInt(JOptionPane.showInputDialog(this, " Enter Frame: ", ""+0));
			} catch(NumberFormatException e) {
			}
			if(frame==-1) return;

			RWFloatArray2D mat = (RWFloatArray2D) RWUtil.getData(struct, "rotationMatrix", frame);

			String axis = JOptionPane.showInputDialog(this, " Enter Axis: ", "Z");
			if(axis!=null) {
				if(axis.toLowerCase().equals("x")) {
					try{
						float angle = Float.parseFloat(JOptionPane.showInputDialog(this, " Enter Angle: ", ""+0));
						angle = (float) Math.toRadians(angle);

						mat.getArray()[0][0] = 1;
						mat.getArray()[0][1] = 0;
						mat.getArray()[0][2] = 0;

						mat.getArray()[1][0] = 0;
						mat.getArray()[1][1] = (float) Math.cos(angle);
						mat.getArray()[1][2] = (float) -Math.sin(angle);

						mat.getArray()[2][0] = 0;
						mat.getArray()[2][1] = (float) Math.sin(angle);
						mat.getArray()[2][2] = (float) Math.cos(angle);
					} catch(NumberFormatException e) {
					}
				} else if(axis.toLowerCase().equals("y")) {
					try{
						float angle = Float.parseFloat(JOptionPane.showInputDialog(this, " Enter Angle: ", ""+0));
						angle = (float) Math.toRadians(angle);

						mat.getArray()[0][0] = (float) Math.cos(angle);
						mat.getArray()[0][1] = 0;
						mat.getArray()[0][2] = (float) Math.sin(angle);

						mat.getArray()[1][0] = 0;
						mat.getArray()[1][1] = 1;
						mat.getArray()[1][2] = 0;

						mat.getArray()[2][0] = (float) -Math.sin(angle);
						mat.getArray()[2][1] = 0;
						mat.getArray()[2][2] = (float) Math.cos(angle);
					} catch(NumberFormatException e) {
					}
				} else if(axis.toLowerCase().equals("z")) {
					try{
						float angle = Float.parseFloat(JOptionPane.showInputDialog(this, " Enter Angle: ", ""+0));
						angle = (float) Math.toRadians(angle);

						mat.getArray()[0][0] = (float) Math.cos(angle);
						mat.getArray()[0][1] = (float) -Math.sin(angle);
						mat.getArray()[0][2] = 0;

						mat.getArray()[1][0] = (float) Math.sin(angle);
						mat.getArray()[1][1] = (float) Math.cos(angle);
						mat.getArray()[1][2] = 0;

						mat.getArray()[2][0] = 0;
						mat.getArray()[2][1] = 0;
						mat.getArray()[2][2] = 1;
					} catch(NumberFormatException e) {
					}
				} else {
					System.out.println("Not an!");
				}
			}
			//scale frame
		} else if(pos==2) {
			RWSection root = getRootSection();
			RWSection frameList = RWUtil.getSection(root, 0x000E);
			RWSection struct = RWUtil.getSection(frameList, 0x0001);

			int frame = -1;
			try{
				frame = Integer.parseInt(JOptionPane.showInputDialog(this, " Enter Frame: ", ""+0));
			} catch(NumberFormatException e) {
			}
			if(frame==-1) return;

			RWFloatArray2D mat = (RWFloatArray2D) RWUtil.getData(struct, "rotationMatrix", frame);
			float scale = Float.parseFloat(JOptionPane.showInputDialog(this, " Enter scale: ", ""+0));

			for(int x1=0; x1<3; x1++) {
				for(int x2=0; x2<3; x2++) {
					mat.getArray()[x1][x2] *= scale;
				}
			}
			//real hack -> wreck soldier cavalry model
		} else if(pos==3) {
			RWSection root = getRootSection();
			RWSection geomList = RWUtil.getSection(root, 0x001A);

			int gpos = -1;
			try{
				gpos = Integer.parseInt(JOptionPane.showInputDialog(this, " Enter Geometry: ", ""+0));
			} catch(NumberFormatException e) {
			}
			if(gpos==-1) return;

			FileGui.setTitle("Open Image Mask");
			FileGui.setStartPath("./");
			FileGui.setAcceptedFiles("png Images", ".png");
			if(!FileGui.showOpenDialog(this)) return;
			File maskFile = FileGui.getFile();

			BufferedImage maskImage = null;
			try {
				maskImage = ImageIO.read(maskFile);
			} catch (IOException e) {
			}
			if(maskImage==null) return;

			int maskWidth = maskImage.getWidth();
			int maskHeight = maskImage.getHeight();
			int[] mask = maskImage.getRGB(0, 0, maskWidth, maskHeight, null, 0, maskWidth);

			if(RWUtil.getSection(geomList, 0x000F, gpos)!=null) {
				RWSection geom = RWUtil.getSection(geomList, 0x000F, gpos);
				RWSection gstruct = RWUtil.getSection(geom, 0x0001);

				RWFloatArray2D uvs = ((RWFloatArray2D) RWUtil.getData(gstruct, "uv"));
				RWFloatArray2D vertexes = ((RWFloatArray2D) RWUtil.getData(gstruct, "vertexes"));

				int count = ((RWInteger) RWUtil.getData(gstruct, "vertexCount")).getInteger();
				for(int i=0; i<count; i++) {
					float u = uvs.getArray()[i][0];
					float v = uvs.getArray()[i][1];

					int xp = (int) Math.round(u*maskWidth);
					int yp = (int) Math.round(v*maskHeight);

					if(mask[xp+yp*maskWidth]!=0xFFFFFFFF) {
						vertexes.getArray()[i][0] = 0;
						vertexes.getArray()[i][1] = 0;
						vertexes.getArray()[i][2] = -100000;
					}

					//custom for grange because uvs are used multiple times in different parts
					//removing the random planks in the corner
					/*if((vertexes.getArray()[i][0] < -240 && vertexes.getArray()[i][1] > 270)
						|| (vertexes.getArray()[i][0] < -230 && vertexes.getArray()[i][1] > 270 && vertexes.getArray()[i][2] > 30 && vertexes.getArray()[i][2] < 100)) {
						vertexes.getArray()[i][0] = 0;
						vertexes.getArray()[i][1] = 0;
						vertexes.getArray()[i][2] = -100000;
					}*/
				}
			}
		} else if(pos==4) {
			FileGui.setTitle("Select File");
			FileGui.setStartPath("./data/graphics/models/");
			FileGui.setAcceptedFiles("Renderware Model Files", ".dff");
			if(FileGui.showOpenDialog(this)) {
				System.out.println(FileGui.getFile());

				CombinedModel combT = new CombinedModel();
				combT.addGeometryFrom(getRootSection(), 0, 0, 0, 0, 1);
				combT.addGeometryFrom(RWUtil.loadRWSection(FileGui.getFile()), 0, 0, 0, 0, 1);
				update(combT.finishClump());
			}
			//copy uvs
		} else if(pos==5) {
			FileGui.setTitle("Select File");
			FileGui.setStartPath("./data/graphics/models/");
			FileGui.setAcceptedFiles("Renderware Model Files", ".dff");
			if(FileGui.showOpenDialog(this)) {
				System.out.println(FileGui.getFile());

				RWSection newSec = RWUtil.loadRWSection(FileGui.getFile());
				
				RWHacks.copyUVasUV2(getRootSection(), newSec);
			}
			//zero uvs
		} else if(pos==6) {
			RWSection root = getRootSection();
			RWSection geomList = RWUtil.getSection(root, 0x001A);

			int gpos = -1;
			try{
				gpos = Integer.parseInt(JOptionPane.showInputDialog(this, " Enter Geometry: ", ""+0));
			} catch(NumberFormatException e) {
			}
			if(gpos==-1) return;

			if(RWUtil.getSection(geomList, 0x000F, gpos)!=null) {
				RWSection geom = RWUtil.getSection(geomList, 0x000F, gpos);
				RWSection gstruct = RWUtil.getSection(geom, 0x0001);

				RWFloatArray2D uvs = ((RWFloatArray2D) RWUtil.getData(gstruct, "uv"));
				RWFloatArray2D uvs2 = null;
				if(RWUtil.getData(gstruct, "uv2")!=null) {
					uvs2 = ((RWFloatArray2D) RWUtil.getData(gstruct, "uv2"));
				}
				RWFloatArray2D vertexes = ((RWFloatArray2D) RWUtil.getData(gstruct, "vertexes"));

				int count = ((RWInteger) RWUtil.getData(gstruct, "vertexCount")).getInteger();
				for(int i=0; i<count; i++) {
					uvs.getArray()[i][0] = 0;
					uvs.getArray()[i][1] = 0;

					if(uvs2!=null) {
						uvs.getArray()[i][0] = 0;
						uvs.getArray()[i][1] = 0;
					}

					vertexes.getArray()[i][0] = 0;
					vertexes.getArray()[i][1] = 0;
					vertexes.getArray()[i][2] = -100000;
				}
			}
			//do code tests
		} else if(pos==7) {
			//test here:
			CombinedModel combT = new CombinedModel();

			combT.addGeometryFrom(RWUtil.loadRWSection(new File("./data/graphics/models/xd_ruinwall1.dff")), -100, 300, 0, (float) Math.toRadians(-45), 0.7f);
			combT.addGeometryFrom(RWUtil.loadRWSection(new File("./data/graphics/models/xd_ruinwall3.dff")), -100, -300, 0, (float) Math.toRadians(-340), 0.7f);
			combT.addGeometryFrom(RWUtil.loadRWSection(new File("./data/graphics/models/xd_ruintower1.dff")), 0, 0, 0, (float) Math.toRadians(-270), 0.7f);
			combT.addGeometryFrom(RWUtil.loadRWSection(new File("./data/graphics/models/xd_ruinfragment3.dff")), -250, 600, 0, (float) Math.toRadians(-180), 1.3f);
			combT.addGeometryFrom(RWUtil.loadRWSection(new File("./data/graphics/models/xd_ruinfragment5.dff")), -200, 640, 0, (float) Math.toRadians(-180), 1.0f);
			combT.addGeometryFrom(RWUtil.loadRWSection(new File("./data/graphics/models/xd_ruinfragment5.dff")), -200, 0, 0, (float) Math.toRadians(-45), 1.3f);
			combT.addGeometryFrom(RWUtil.loadRWSection(new File("./data/graphics/models/xd_ruinsmalltower3.dff")), -200, -600, 0, (float) Math.toRadians(-45), 0.6f);

			update(combT.finishClump());

			//try the same thing automated
			try(BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(new File("./data/Ruins2.lua"))))) {
				Iterator<String> it = reader.lines().iterator();

				while(it.hasNext()) {
					//parse name
					String name = it.next();
					name = name.replace(" ", "");
					name = name.substring(0, name.indexOf('='));
					name = name + "_Ruin";
					System.out.println("Creating "+name);

					CombinedModel comb = new CombinedModel();

					//parse models
					String line = "";
					while(!(line=it.next()).equals("    },")) {
						//parse model input
						String[] split = line.split(",");

						String xS = split[0].split("=")[1].replace(" ", "");
						String yS = split[1].split("=")[1].replace(" ", "");
						String rotS = split[2].split("=")[1].replace(" ", "");
						String model = split[3].split("=")[1].replace(" ", "").replace("\"", "").toLowerCase();
						String scaleS = split[4].split("=")[1].replace(" ", "").replace("}", "");

						float x = Float.parseFloat(xS);
						float y = Float.parseFloat(yS);
						float rot = Float.parseFloat(rotS);
						model = "./data/graphics/models/"+model+".dff";
						float scale = Float.parseFloat(scaleS);

						comb.addGeometryFrom(RWUtil.loadRWSection(new File(model)), x, y, 0, (float) Math.toRadians(-rot), scale);
					}

					//save model
					File saveFile = new File("./data/out/combined/"+name+".dff");
					if(!saveFile.getParentFile().exists()) saveFile.getParentFile().mkdirs();

					if(!saveFile.exists()) {
						saveFile.createNewFile();
					}

					//should always produce the exact binary file
					try(EndianCorrectOutputStream dos = new EndianCorrectOutputStream(new FileOutputStream(saveFile), false)) {
						comb.finishClump().write(dos);
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void doBatchProcess(File descFile) {
		try(BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(descFile)))) {
			File outFolder = new File("./data/out/batch/");
			if(!outFolder.exists()) outFolder.mkdirs();

			String line = "";
			while((line=reader.readLine())!=null) {
				//process command
				String[] split = line.split(" ");
				if(split.length<2) {
					System.out.println("Warning: Invalid format: "+line);
					continue;
				}

				//find fitting file
				File modelFile = new File(outFolder, split[0]);
				if(!modelFile.exists()) {
					//try copy model from data
					File dataModelFile = new File("./data/graphics/models/", split[0]);
					if(!dataModelFile.exists()) {
						System.out.println("Warning: Couldn't find "+split[0]);
						continue;
					}
					Files.copy(dataModelFile.toPath(), modelFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
				}
				RWSection sec = RWUtil.loadRWSection(modelFile);

				//switch on command
				switch(split[1].toLowerCase()) {
				case "changetex":
					if(split.length!=4) {
						System.out.println("Warning: changetex requires two arguments!");
						continue;
					}
					String fromTex = split[2].toLowerCase();
					String toTex = split[3];

					//Do texture change
					//Loop through all geoms with all their materials
					RWSection geomList = RWUtil.getSection(sec, 0x001A);
					int gpos = 0;
					while(RWUtil.getSection(geomList, 0x000F, gpos)!=null) {
						RWSection geom = RWUtil.getSection(geomList, 0x000F, gpos);
						RWSection matList = RWUtil.getSection(geom, 0x0008);

						int mpos = 0;
						while(RWUtil.getSection(matList, 0x0007, mpos)!=null) {
							//find texture path in material
							RWSection mat = RWUtil.getSection(matList, 0x0007, mpos);
							RWSection tex = RWUtil.getSection(mat, 0x0006);
							RWSection texString = RWUtil.getSection(tex, 0x0002, 0);
							RWString string = (RWString) RWUtil.getData(texString, "string");

							boolean fits = false;
							if(string.getString().toLowerCase().equals(fromTex)) fits = true;
							if(fromTex.equals("?")) {
								int result = JOptionPane.showConfirmDialog(this, "Replace "+string.getString()+" with "+toTex+"?", split[0], JOptionPane.YES_NO_OPTION);
								fits = result == JOptionPane.YES_OPTION;
							}

							//change texture path
							if(fits) {
								if(!string.tryEdit(toTex)) {
									System.out.println("Warning: Pathchange unsuccessful!");
								}
							}

							mpos++;
						}

						gpos++;
					}

					break;
				default:
					System.out.println("Warning: Unknown command: "+split[1]);
					continue;
				}

				//save modified section
				//should always produce the exact binary file
				try(EndianCorrectOutputStream dos = new EndianCorrectOutputStream(new FileOutputStream(modelFile), false)) {
					sec.write(dos);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} catch (FileNotFoundException e) {
		} catch (IOException e) {
		}
	}





	@Override
	public void windowClosed(WindowEvent arg0) {}

	@Override
	public void windowActivated(WindowEvent arg0) {}

	@Override
	public void windowDeactivated(WindowEvent arg0) {}

	@Override
	public void windowDeiconified(WindowEvent arg0) {}

	@Override
	public void windowIconified(WindowEvent arg0) {}

	@Override
	public void windowOpened(WindowEvent arg0) {}

	@Override
	public void mouseEntered(MouseEvent arg0) {}

	@Override
	public void mouseExited(MouseEvent arg0) {}

	@Override
	public void mousePressed(MouseEvent arg0) {}

	@Override
	public void mouseReleased(MouseEvent arg0) {}
}
