package me.andre111.s5modeller.gui;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BoxLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import me.andre111.s5modeller.rwbs.RWInfo;
import me.andre111.s5modeller.rwbs.RWSection;
import me.andre111.s5modeller.rwbs.RWSectionInfoList;

public class SectionGui extends JPanel {
	private static final long serialVersionUID = -2490032593576225182L;
	
	private S5MGui gui;
	private RWSection section;

	public SectionGui(S5MGui gui) {
		this.gui = gui;
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.setMinimumSize(new Dimension(400, 20));
		this.setPreferredSize(new Dimension(400, 20));
	}
	
	public void setSection(RWSection sec) {
		section = sec;
		if(sec==null) {
			reset();
			return;
		}
		
		this.removeAll();
		
		JTextArea nameField = new JTextArea(
				section.getName()+" ("+section.getHexID()+") \n"+
				(RWSectionInfoList.hasSectionDescription(section) ? " \n" : "Dataformat unknown! \n")
		);
		nameField.setEditable(false);
		this.add(nameField);
		
		JScrollPane pane = new JScrollPane();
		pane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		pane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		JPanel gridPanel = new JPanel();
		gridPanel.setLayout(new GridLayout(0, 2));
		{
			for(RWInfo info : section.getData()) {
				if(info.getData() instanceof RWSection) {
					JTextField dataNameField = new JTextField("SubSection - "+info.getData().getName());
					dataNameField.setEditable(false);
					gridPanel.add(dataNameField);
					
					JTextField dataValueField = new JTextField("");
					dataValueField.setEditable(false);
					gridPanel.add(dataValueField);
				} else {
					JTextField dataNameField = new JTextField(info.getName()+" - "+info.getData().getName());
					dataNameField.setEditable(false);
					gridPanel.add(dataNameField);
					
					JTextField dataValueField = new JTextField(info.getData().valueToString());
					dataValueField.setEditable(false);
					dataValueField.addMouseListener(new MouseAdapter(){
						@Override
						public void mouseClicked(MouseEvent e) {
							editData(info);
						}
					});
					gridPanel.add(dataValueField);
				}
			}
		}
		pane.getViewport().add(gridPanel);
		this.add(pane);
		
		gui.validate();
	}
	
	public void editData(RWInfo info) {
		System.out.println("Trying to edit: "+info.getData());
		if(info.getData().canEdit()) {
			String newValue = JOptionPane.showInputDialog(this, " Enter new Value: ", ""+info.getData().valueToString());
			if(newValue==null || !info.getData().tryEdit(newValue)) {
				System.out.println("Warning: Invalid value!");
			} else {
				gui.recreate();
			}
		}
	}
	
	public void reset() {
		this.removeAll();
	}
}
