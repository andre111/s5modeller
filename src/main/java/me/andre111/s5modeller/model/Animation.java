package me.andre111.s5modeller.model;

import java.util.ArrayList;
import java.util.List;

import me.andre111.s5modeller.rwbs.RWSection;
import me.andre111.s5modeller.rwbs.data.RWFloat;
import me.andre111.s5modeller.rwbs.data.RWInteger;
import me.andre111.s5modeller.rwbs.data.RWShort;
import me.andre111.s5modeller.rwbs.util.RWUtil;

//TODO: The name bone is not completely right, as this applies to frames and models without a skin
public class Animation {
	private List<KeyFrame> keyFrames;
	private int boneCount;
	
	//the root bone to apply the animation to, can be something else than 0
	private int baseBoneID;
	
	private float maxTime;
	
	public Animation(RWSection animSection, int baseBoneID) {
		if(animSection.getID()!=0x001B) throw new IllegalArgumentException("Provided Section is not a Animation");
		
		keyFrames = new ArrayList<>();
		boneCount = 0;
		
		this.baseBoneID = baseBoneID;
		
		int type = ((RWInteger) RWUtil.getData(animSection, "typeFlagUnknown")).getInteger();
		int frameCount = ((RWInteger) RWUtil.getData(animSection, "numFrames")).getInteger();
		
		float offsetX = 0;
		float offsetY = 0;
		float offsetZ = 0;
		float scaleX = 1;
		float scaleY = 1;
		float scaleZ = 1;
		if(type==2) {
			offsetX = ((RWFloat) RWUtil.getData(animSection, "offsetX")).getFloat();
			offsetY = ((RWFloat) RWUtil.getData(animSection, "offsetY")).getFloat();
			offsetZ = ((RWFloat) RWUtil.getData(animSection, "offsetZ")).getFloat();
			scaleX = ((RWFloat) RWUtil.getData(animSection, "scaleX")).getFloat();
			scaleY = ((RWFloat) RWUtil.getData(animSection, "scaleY")).getFloat();
			scaleZ = ((RWFloat) RWUtil.getData(animSection, "scaleZ")).getFloat();
		}
		
		for(int i=0; i<frameCount; i++) {
			int parent = ((RWInteger) RWUtil.getData(animSection, "prevFrame", i)).getInteger();
			
			if(type==1) {
				if(parent>=0) parent = parent / 36; //TODO: this should really not be hard coded
				
				float time = ((RWFloat) RWUtil.getData(animSection, "time", i)).getFloat();
				
				float rotX = ((RWFloat) RWUtil.getData(animSection, "orientationX", i)).getFloat();
				float rotY = ((RWFloat) RWUtil.getData(animSection, "orientationY", i)).getFloat();
				float rotZ = ((RWFloat) RWUtil.getData(animSection, "orientationZ", i)).getFloat();
				float rotW = ((RWFloat) RWUtil.getData(animSection, "orientationW", i)).getFloat();
				float transX = ((RWFloat) RWUtil.getData(animSection, "translationX", i)).getFloat();
				float transY = ((RWFloat) RWUtil.getData(animSection, "translationY", i)).getFloat();
				float transZ = ((RWFloat) RWUtil.getData(animSection, "translationZ", i)).getFloat();
				
				keyFrames.add(new KeyFrame(parent, time, rotX, rotY, rotZ, rotW, transX, transY, transZ));
			} else if(type==2) {
				if(parent>=0) parent = parent / 24; //TODO: this should really not be hard coded
				
				float time = ((RWFloat) RWUtil.getData(animSection, "time", i)).getFloat();
				
				float rotX = convertShortToFloat(((RWShort) RWUtil.getData(animSection, "orientationX", i)).getShort());
				float rotY = convertShortToFloat(((RWShort) RWUtil.getData(animSection, "orientationY", i)).getShort());
				float rotZ = convertShortToFloat(((RWShort) RWUtil.getData(animSection, "orientationZ", i)).getShort());
				float rotW = convertShortToFloat(((RWShort) RWUtil.getData(animSection, "orientationW", i)).getShort());
				float transX = convertShortToFloat(((RWShort) RWUtil.getData(animSection, "translationX", i)).getShort());
				float transY = convertShortToFloat(((RWShort) RWUtil.getData(animSection, "translationY", i)).getShort());
				float transZ = convertShortToFloat(((RWShort) RWUtil.getData(animSection, "translationZ", i)).getShort());
				
				transX = transX * scaleX + offsetX;
				transY = transY * scaleY + offsetY;
				transZ = transZ * scaleZ + offsetZ;
				
				keyFrames.add(new KeyFrame(parent, time, rotX, rotY, rotZ, rotW, transX, transY, transZ));
			} else {
				throw new RuntimeException("Unknown animation type "+type);
			}
		}
		
		maxTime = keyFrames.get(keyFrames.size()-1).time;
		
		//basic validation (first boneCount frames need time=0 all others need parent>=0)
		for(int i=0; i<frameCount; i++) {
			if(i < boneCount) {
				if(keyFrames.get(i).time!=0) throw new RuntimeException("Broken Animation?");
			} else {
				if(keyFrames.get(i).parent<0) throw new RuntimeException("Broken Animation?");
			}
			if(i < 0) {
				if(keyFrames.get(i-1).time>keyFrames.get(i).time) throw new RuntimeException("Broken Animation?");
			}
			//TODO: last frame per bone should have time=maxTime
		}
	}
	
	//TODO: To support importing to the compressed format I would have to write the reverse of this operation somehow, so just import to type 1 for now
	private float convertShortToFloat(short s) {
		//TODO: short should be unsigned but currently it isnt , fix that when rewriting the rwstream code 
		int value = s & 0xffff;
		
		//convert to floating point
		int bits = (value & 0x8000) << 16;
		if((value & 0x7fff) != 0) {
			bits |= ((value & 0x7800)<<12) + 0x38000000;
			bits |= (value & 0x07ff)<<12;
		}
		
		return Float.intBitsToFloat(bits);
	}
	
	public float getMaxTime() {
		return maxTime;
	}
	
	public int getBoneCount() {
		return boneCount;
	}
	
	public int getBaseBoneID() {
		return baseBoneID;
	}
	
	public float[][] getBoneMatrix(int bone, float time) {
		KeyFrame frame1 = null;
		KeyFrame frame2 = null;
		for(KeyFrame frame : keyFrames) {
			if(frame.bone==bone) {
				if(frame.time < time) {
					frame1 = frame;
				} else {
					frame2 = frame;
					break;
				}
			}
		}
		if(frame1==null || frame2==null) throw new RuntimeException("Failed to calculate bone transformation");
		
		//interpolate
		float percentage = (time-frame1.time) / (frame2.time-frame1.time);
		
		
		Quaternion quat1 = new Quaternion(frame1.rotX, frame1.rotY, frame1.rotZ, frame1.rotW);
		Quaternion quat2 = new Quaternion(frame2.rotX, frame2.rotY, frame2.rotZ, frame2.rotW);
		quat1.slerp(quat2, percentage);
		quat1.normalize();
		float transX = (1-percentage)*frame1.transX + percentage*frame2.transX;
		float transY = (1-percentage)*frame1.transY + percentage*frame2.transY;
		float transZ = (1-percentage)*frame1.transZ + percentage*frame2.transZ;
		
		//convert to matrix
		float[][] matrix = quat1.toMat4();
		matrix[3][0] = transX;
		matrix[3][1] = transY;
		matrix[3][2] = transZ;
		
		return matrix;
	}
	
	public KeyFrame getKeyFrame(int index) {
		return keyFrames.get(index);
	}
	
	private int getNextBoneNumber() {
		return boneCount++;
	}
	
	private final class KeyFrame {
		private int parent;
		private float time;
		private float rotX;
		private float rotY;
		private float rotZ;
		private float rotW;
		private float transX;
		private float transY;
		private float transZ;
		
		private int bone;
		
		public KeyFrame(int parent, float time, float rotX, float rotY, float rotZ, float rotW, float transX, float transY, float transZ) {
			this.parent = parent;
			this.time = time;
			this.rotX = rotX;
			this.rotY = rotY;
			this.rotZ = rotZ;
			this.rotW = rotW;
			this.transX = transX;
			this.transY = transY;
			this.transZ = transZ;
			
			if(parent < 0) {
				this.bone = getNextBoneNumber();
			} else {
				this.bone = getKeyFrame(parent).bone;
			}
		}
	}
}
