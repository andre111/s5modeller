package me.andre111.s5modeller.model;

import java.io.File;
import java.nio.FloatBuffer;
import java.util.ArrayList;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.util.vector.Matrix4f;

import me.andre111.s5modeller.dds.DDSLoader;
import me.andre111.s5modeller.rwbs.RWData;
import me.andre111.s5modeller.rwbs.RWInfo;
import me.andre111.s5modeller.rwbs.RWSection;
import me.andre111.s5modeller.rwbs.data.RWByteArray2D;
import me.andre111.s5modeller.rwbs.data.RWFloat;
import me.andre111.s5modeller.rwbs.data.RWFloatArray2D;
import me.andre111.s5modeller.rwbs.data.RWInteger;
import me.andre111.s5modeller.rwbs.data.RWIntegerArray2D;
import me.andre111.s5modeller.rwbs.data.RWString;
import me.andre111.s5modeller.rwbs.util.RWUtil;
import me.andre111.s5modeller.util.MatUtil;

public class Model {
	private RWSection root;
	private RWSection frameList;
	private RWSection geometryList;
	private RWSection frameListStruct;

	private ArrayList<RWSection> atomics = new ArrayList<RWSection>();
	public ArrayList<ModelPart> parts = new ArrayList<ModelPart>();

	//for display purposes
	private int selectedObject = -1;
	private FloatBuffer transformationBuffer = BufferUtils.createFloatBuffer(16);

	public Model(RWSection rootSection) {
		root = rootSection;
		reload();
	}

	public void reload() {
		atomics.clear();
		parts.clear();

		frameList = RWUtil.getSection(root, 0x000E);
		geometryList = RWUtil.getSection(root, 0x001A);

		if(frameList!=null && geometryList!=null) {
			frameListStruct = RWUtil.getSection(frameList, 0x0001);
			
			for(RWInfo data : root.getData()) {
				if(data.getData() instanceof RWSection) {
					RWSection child = (RWSection) data.getData();

					if(child.getID()==0x0014) {
						addAtomic(child);
					}
				}
			}
		} else {
			System.out.println("Provided RWBS File is not a supported model file!");
		}
	}

	private void addAtomic(RWSection atomic) {
		RWSection struct = RWUtil.getSection(atomic, 0x0001);

		if(struct!=null) {
			//get required elements
			int frameIndex = ((RWInteger) RWUtil.getData(struct, "frameIndex")).getInteger();
			int geometryIndex = ((RWInteger) RWUtil.getData(struct, "geometryIndex")).getInteger();
			
			//Get Mesh information
			RWSection geometry = RWUtil.getSection(geometryList, 0x000F, geometryIndex);
			RWSection geometryStruct = RWUtil.getSection(geometry, 0x0001);

			RWSection materialList = RWUtil.getSection(geometry, 0x0008);

			float[][] vertexes = ((RWFloatArray2D) RWUtil.getData(geometryStruct, "vertexes")).getArray();
			float[][] uvs = new float[0][0];
			float[][] uvs2 = new float[0][0];
			float[][] normals = new float[0][0];
			int[][] colors = new int[0][0];

			RWData uvData = RWUtil.getData(geometryStruct, "uv");
			if(uvData!=null) {
				uvs = ((RWFloatArray2D) uvData).getArray();
			}
			RWData uv2Data = RWUtil.getData(geometryStruct, "uv2");
			if(uv2Data!=null) {
				uvs2 = ((RWFloatArray2D) uv2Data).getArray();
			}
			RWData normalData = RWUtil.getData(geometryStruct, "normals");
			if(normalData!=null) {
				normals = ((RWFloatArray2D) normalData).getArray();
			}
			RWData colorData = RWUtil.getData(geometryStruct, "matColor");
			if(colorData!=null) {
				colors = ((RWByteArray2D) colorData).getArray();
			}

			//generate Model parts
			RWSection binMesh = RWUtil.getSection(RWUtil.getSection(geometry, 0x003), 0x050E);
			if(binMesh != null) {
				int meshCount = ((RWInteger) RWUtil.getData(binMesh, "meshCount")).getInteger();
				boolean isTristrip = ((RWInteger) RWUtil.getData(binMesh, "geometryTristrip")).getInteger()==1;

				for(int m=0; m<meshCount; m++) {
					int indexCount = ((RWInteger) RWUtil.getData(binMesh, "meshIndexCount", m)).getInteger();
					int materialIndex = ((RWInteger) RWUtil.getData(binMesh, "materialIndex", m)).getInteger();

					RWIntegerArray2D indices = (RWIntegerArray2D) RWUtil.getData(binMesh, "indices", m);

					//generate basic geometry
					ModelPart part = generatePartFromBinMeshMesh(geometry, atomics.size(), frameIndex, geometryIndex, vertexes, uvs, normals, colors, isTristrip, indexCount, materialIndex, indices.getArray());
					ModelPart part2 = generatePartFromBinMeshMesh(geometry, atomics.size(), frameIndex, geometryIndex, vertexes, (uvs2.length>0 ? uvs2 : uvs), normals, colors, isTristrip, indexCount, materialIndex, indices.getArray());

					if(colors.length>0) {
						part.setHasColors(true);
						part2.setHasColors(true);
						System.out.println("has colors");
					}
					if(normals.length>0) {
						part.setHasNormals(true);
						part2.setHasNormals(true);
					}

					//add material information
					RWSection material = RWUtil.getSection(materialList, 0x0007, materialIndex);

					RWSection texture = RWUtil.getSection(material, 0x0006); //TODO - handle multiple textures
					if(texture!=null) {
						RWSection textureName = RWUtil.getSection(texture, 0x0002);
	
						String texName = ((RWString) RWUtil.getData(textureName, "string")).getString();
						part.setTextureName(texName);
						part.setTexture(getTextureID(texName));
					} else {
						System.out.println("Warning: Material has no Texture - what do? Game would probably crash trying to load this model.");
					}

					//check for dual map
					//boolean hasDualPassMap = false;

					RWSection matExt = RWUtil.getSection(material, 0x0003);
					RWSection matEffects = RWUtil.getSection(matExt, 0x0120);
					if(matEffects!=null) {
						//TODO - add Dualpass Map Effects
					}

					//add parts
					parts.add(part);

					//if(hasDualPassMap) {
					//	parts.add(part2);
					//}
				}
			} else {
				System.out.println("Model uses not yet implemented format!");
			}
		}

		atomics.add(atomic);
	}
	
	private ModelPart generatePartFromBinMeshMesh(RWSection geometry, int objectID, int frameIndex, int geometryIndex, float[][] vertexes, float[][] uvs, float[][] normals, int[][] colors, boolean istrisrip, int indexCount, int materialIndex, int[][] indices) {
		ModelPart part = new ModelPart();
		part.setObjectID(objectID);
		part.setFrame(frameIndex);
		part.setGeometry(geometryIndex);

		//get count of all existing vertexes
		int realCount = 0;
		if(istrisrip) {
			//convert from tristrip to triangle list
			for(int i=2; i<indexCount; i++) {
				int indexM0 = indices[i][0];
				int indexM1 = indices[i-1][0];
				int indexM2 = indices[i-2][0];

				if(indexM0<vertexes.length && indexM1<vertexes.length && indexM2<vertexes.length) {
					//Check for degeneration
					if(checkTwoSame(vertexes[indexM2], vertexes[indexM1], vertexes[indexM0])) {
						continue;
					}
					realCount+=3;
				}
			}
		} else {
			for(int i=0; i<indexCount; i++) {
				int index = indices[i][0];
	
				if(index<vertexes.length) {
					realCount++;
				}
			}
		}

		//create vertexes
		int[] partIndices = new int[realCount];
		float[][] partVertexes = new float[realCount][4];
		float[][] partUVs = new float[realCount][2];
		float[][] partNormals = new float[realCount][3];
		float[][] partColors = new float[realCount][4];

		int count = 0;
		if(istrisrip) {
			//convert from tristrip to triangle list
			for(int i=2; i<indexCount; i++) {
				int[] indicesLocal = new int[3];
				if(i%2==0) {
					indicesLocal[0] = indices[i-2][0];
					indicesLocal[1] = indices[i-1][0];
					indicesLocal[2] = indices[i-0][0];
				} else {
					indicesLocal[0] = indices[i-0][0];
					indicesLocal[1] = indices[i-1][0];
					indicesLocal[2] = indices[i-2][0];
				}

				if(indicesLocal[0]<vertexes.length && indicesLocal[1]<vertexes.length && indicesLocal[2]<vertexes.length) {
					//Check for degeneration
					if(checkTwoSame(vertexes[indicesLocal[0]], vertexes[indicesLocal[1]], vertexes[indicesLocal[2]])) {
						continue;
					}
					
					for(int j=0; j<indicesLocal.length; j++) {
						partIndices[count] = indicesLocal[j];
						
						float[] vertex = {
								vertexes[indicesLocal[j]][0],
								vertexes[indicesLocal[j]][1],
								vertexes[indicesLocal[j]][2],
								1
						};
						partVertexes[count] = vertex;
						
						if(indicesLocal[j]<uvs.length) {
							partUVs[count][0] = uvs[indicesLocal[j]][0];
							partUVs[count][1] = uvs[indicesLocal[j]][1];
						}
		
						if(indicesLocal[j]<normals.length) {
							partNormals[count][0] = normals[indicesLocal[j]][0];
							partNormals[count][1] = normals[indicesLocal[j]][1];
							partNormals[count][2] = normals[indicesLocal[j]][2];
						}
		
						if(indicesLocal[j]<colors.length) {
							partColors[count][0] = colors[indicesLocal[j]][0] / 255f;
							partColors[count][1] = colors[indicesLocal[j]][1] / 255f;
							partColors[count][2] = colors[indicesLocal[j]][2] / 255f;
							partColors[count][3] = colors[indicesLocal[j]][3] / 255f;
						}
		
						count++;
					}
				}
			}
		} else {
			for(int i=0; i<indexCount; i++) {
				int index = indices[i][0];
	
				if(index<vertexes.length) {
					partIndices[count] = index;
					
					float[] vertex = {
							vertexes[index][0],
							vertexes[index][1],
							vertexes[index][2],
							1
					};
	
					partVertexes[count] = vertex;
	
					if(index<uvs.length) {
						partUVs[count][0] = uvs[index][0];
						partUVs[count][1] = uvs[index][1];
					}
	
					if(index<normals.length) {
						partNormals[count][0] = normals[index][0];
						partNormals[count][1] = normals[index][1];
						partNormals[count][2] = normals[index][2];
					}
	
					if(index<colors.length) {
						partColors[count][0] = colors[index][0] / 255f;
						partColors[count][1] = colors[index][1] / 255f;
						partColors[count][2] = colors[index][2] / 255f;
						partColors[count][3] = colors[index][3] / 255f;
					}
	
					count++;
				}
			}
		}

		part.setVertexCount(realCount);
		part.setOriginalIndices(partIndices);
		part.setVertexes(partVertexes);
		part.setUVs(partUVs);
		part.setNormals(partNormals);
		part.setColors(partColors);

		return part;
	}
	
	private boolean checkTwoSame(float[] v1, float[] v2, float[] v3) {
		if(checkSame(v1, v2)) return true;
		if(checkSame(v1, v3)) return true;
		if(checkSame(v2, v3)) return true;

		return false;
	}

	private boolean checkSame(float[] v1, float[] v2) {
		if(v1.length!=v2.length) return false;

		float maxDist = 0.001f;
		for(int i=0; i<v1.length; i++) {
			float dist = Math.abs(v1[i]-v2[i]);
			if(dist>maxDist) return false;
		}

		return true;
	}

	private int getTextureID(String name) {
		int texId = -1;

		DDSLoader loader = new DDSLoader();
		String texFile = "";
		try {
			texFile = "./data/graphics/textures/"+name.toLowerCase()+".dds";
			if(!new File(texFile).exists()) texFile = "./data/out/graphics/textures/"+name.toLowerCase()+".dds";

			texId = loader.loadDDSFile(texFile);
		} catch(Exception e) {
			System.out.println("Texture Error!");
			if(!new File(texFile).exists()) System.out.println("Missing: "+texFile);
		}

		return texId;
	}




	private Matrix4f getFrameTransformation(RWSection frameListStruct, int frame, Matrix4f childMatrix) {
		//calculate (absolute) transformation matrix of bone
		Matrix4f transformation = new Matrix4f();
		transformation.setIdentity();

		Matrix4f matrix = new Matrix4f();
		matrix.setIdentity();

		float[][] rotMatrix = ((RWFloatArray2D) RWUtil.getData(frameListStruct, "rotationMatrix", frame)).getArray();
		float offX = ((RWFloat) RWUtil.getData(frameListStruct, "coordinateOffsetX", frame)).getFloat();
		float offY = ((RWFloat) RWUtil.getData(frameListStruct, "coordinateOffsetY", frame)).getFloat();
		float offZ = ((RWFloat) RWUtil.getData(frameListStruct, "coordinateOffsetZ", frame)).getFloat();
		
		//TODO: Tests
		matrix.m00 = rotMatrix[0][0];
		matrix.m01 = rotMatrix[0][1];
		matrix.m02 = rotMatrix[0][2];
		matrix.m03 = 0;
		matrix.m10 = rotMatrix[1][0];
		matrix.m11 = rotMatrix[1][1];
		matrix.m12 = rotMatrix[1][2];
		matrix.m13 = 0;
		matrix.m20 = rotMatrix[2][0];
		matrix.m21 = rotMatrix[2][1];
		matrix.m22 = rotMatrix[2][2];
		matrix.m23 = 0;
		matrix.m30 = offX;
		matrix.m31 = offY;
		matrix.m32 = offZ;
		matrix.m33 = 1;
		
			
		if(childMatrix!=null) {
			Matrix4f.mul(matrix, childMatrix, transformation);
		} else {
			transformation = matrix;
		}

		int parentFrame = ((RWInteger) RWUtil.getData(frameListStruct, "parentFrame", frame)).getInteger();
		if(parentFrame>=0) {
			return getFrameTransformation(frameListStruct, parentFrame, transformation);
		} else {
			return transformation;
		}
	}

	public void draw(ModelVertexModifier modifier) {
		float[] vertex = new float[4];
		
		for(ModelPart part : parts) {
			GL13.glActiveTexture(GL13.GL_TEXTURE0);
			GL11.glEnable(GL11.GL_TEXTURE_2D);

			if(part.getTexture()!=-1) GL11.glBindTexture(GL11.GL_TEXTURE_2D, part.getTexture());
			else GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);

			GL11.glLoadName(part.getObjectID() + 1);
			
			//TODO: transform based on frame
			GL11.glPushMatrix();

			transformationBuffer.rewind();
			getFrameTransformation(frameListStruct, part.getFrame(), null).store(transformationBuffer);
			transformationBuffer.rewind();
			GL11.glMultMatrix(transformationBuffer);

			GL11.glBegin(GL11.GL_TRIANGLES);

			for(int i=0; i<part.getVertexCount(); i++) {
				GL11.glTexCoord2f(part.getUVs()[i][0], part.getUVs()[i][1]);
				if(part.isHasNormals()) {
					GL11.glNormal3f(part.getNormals()[i][0], part.getNormals()[i][1], part.getNormals()[i][2]);
				}
				if(part.isHasColors() || selectedObject==part.getObjectID()) {
					//TODO - is using color right here
					float r = (part.isHasColors() ? part.getColors()[i][0]*0.2f : 0.2f);
					float g = (part.isHasColors() ? part.getColors()[i][1]*0.2f : 0.2f);
					float b = (part.isHasColors() ? part.getColors()[i][2]*1f : 1f);

					GL11.glColor3f(r, g, b);
				}
				
				vertex[0] = part.getVertexes()[i][0];
				vertex[1] = part.getVertexes()[i][1];
				vertex[2] = part.getVertexes()[i][2];
				vertex[3] = 1;
				if(modifier!=null) {
					vertex = modifier.modifyVertex(vertex, part.getOriginalIndices()[i]);
				}
				if(vertex[3]!=1) throw new RuntimeException("Invalid vertex");
				GL11.glVertex3f(vertex[0], vertex[1], vertex[2]);

				if(part.isHasColors() || selectedObject==part.getObjectID()) {
					GL11.glColor3f(1, 1, 1);
				}
			}

			GL11.glEnd();
			
			GL11.glPopMatrix();

			GL11.glLoadName(0);

			GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);

			GL11.glDisable(GL11.GL_TEXTURE_2D);
		}
	}
	
	public void drawWireframe(ModelVertexModifier modifier) {
		float[] vertex = new float[4];
		
		GL11.glPolygonMode( GL11.GL_FRONT_AND_BACK, GL11.GL_LINE );
		GL11.glDisable(GL11.GL_CULL_FACE);
		GL11.glLineWidth(2f);
		GL11.glColor3f(1, 1, 1);
		for(ModelPart part : parts) {
			GL11.glLoadName(part.getObjectID() + 1);

			GL11.glBegin(GL11.GL_TRIANGLES);

			for(int i=0; i<part.getVertexCount(); i++) {
				vertex[0] = part.getVertexes()[i][0];
				vertex[1] = part.getVertexes()[i][1];
				vertex[2] = part.getVertexes()[i][2];
				vertex[3] = 1;
				if(modifier!=null) {
					vertex = modifier.modifyVertex(vertex, part.getOriginalIndices()[i]);
				}
				if(vertex[3]!=1) throw new RuntimeException("Invalid vertex");
				GL11.glVertex3f(vertex[0], vertex[1], vertex[2]);
			}

			GL11.glEnd();

			GL11.glLoadName(0);
		}
		GL11.glLineWidth(1f);
		GL11.glPolygonMode( GL11.GL_FRONT_AND_BACK, GL11.GL_FILL );
		GL11.glEnable(GL11.GL_CULL_FACE);
	}

	public void setSelection(int sel) {
		if(sel<0 || sel>=parts.size()) {
			selectedObject = -1;
		} else {
			selectedObject = sel;
		}
	}
	
	public ModelPart getSelection() {
		if(selectedObject!=-1) {
			return parts.get(selectedObject);
		}
		return null;
	}

	public void move(float x, float y, float z) {
		if(selectedObject==-1) return;

		RWSection atomic = atomics.get(selectedObject);
		RWSection struct = RWUtil.getSection(atomic, 0x0001);

		if(struct!=null) {
			//get required elements
			int frameIndex = ((RWInteger) RWUtil.getData(struct, "frameIndex")).getInteger();

			//RWData rotationMatrix = RWUtil.getData(RWUtil.getSection(frameList, 0x001), "rotationMatrix", frameIndex);
			RWFloat offsetX = (RWFloat) RWUtil.getData(RWUtil.getSection(frameList, 0x001), "coordinateOffsetX", frameIndex);
			RWFloat offsetY = (RWFloat) RWUtil.getData(RWUtil.getSection(frameList, 0x001), "coordinateOffsetY", frameIndex);
			RWFloat offsetZ = (RWFloat) RWUtil.getData(RWUtil.getSection(frameList, 0x001), "coordinateOffsetZ", frameIndex);

			offsetX.setFloat(offsetX.getFloat()+x);
			offsetY.setFloat(offsetY.getFloat()+y);
			offsetZ.setFloat(offsetZ.getFloat()+z);

			reload();
		}
	}

	public void rotate(float rot) {
		if(selectedObject==-1) return;

		RWSection atomic = atomics.get(selectedObject);
		RWSection struct = RWUtil.getSection(atomic, 0x0001);

		if(struct!=null) {
			//get required elements
			int frameIndex = ((RWInteger) RWUtil.getData(struct, "frameIndex")).getInteger();

			RWFloatArray2D rotationMatrix = (RWFloatArray2D) RWUtil.getData(RWUtil.getSection(frameList, 0x001), "rotationMatrix", frameIndex);
			RWFloatArray2D resultMatrix = MatUtil.multiply(rotationMatrix, MatUtil.getRotMatrix(rot, "Z"));

			//copy values
			for(int x=0; x<3; x++) {
				for(int y=0; y<3; y++) {
					rotationMatrix.getArray()[x][y] = resultMatrix.getArray()[x][y];
				}
			}

			reload();
		}
	}

	public void scale(float scale) {
		if(selectedObject==-1) return;

		RWSection atomic = atomics.get(selectedObject);
		RWSection struct = RWUtil.getSection(atomic, 0x0001);

		if(struct!=null) {
			//get required elements
			int frameIndex = ((RWInteger) RWUtil.getData(struct, "frameIndex")).getInteger();

			RWFloatArray2D rotationMatrix = (RWFloatArray2D) RWUtil.getData(RWUtil.getSection(frameList, 0x001), "rotationMatrix", frameIndex);
			RWFloatArray2D resultMatrix = MatUtil.multiply(rotationMatrix, MatUtil.getScaleMatrix(scale, scale, scale));

			//copy values
			for(int x=0; x<3; x++) {
				for(int y=0; y<3; y++) {
					rotationMatrix.getArray()[x][y] = resultMatrix.getArray()[x][y];
				}
			}

			reload();
		}
	}
}
