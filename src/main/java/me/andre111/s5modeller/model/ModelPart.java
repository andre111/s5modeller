package me.andre111.s5modeller.model;

public class ModelPart {
	private int vertexCount;
	
	private int[] originalIndices;
	private float[][] vertexes;
	private float[][] uvs;
	
	private float[][] normals;
	private boolean hasNormals;
	
	private float[][] colors;
	private boolean hasColors;
	
	private String textureName;
	private int texture;
	
	//only for internal reasons
	private int objectID;
	private int frame;
	private int geometry;
	
	public int getVertexCount() {
		return vertexCount;
	}
	public void setVertexCount(int vertexCount) {
		this.vertexCount = vertexCount;
	}
	public int[] getOriginalIndices() {
		return originalIndices;
	}
	public void setOriginalIndices(int[] originalIndices) {
		this.originalIndices = originalIndices;
	}
	public float[][] getVertexes() {
		return vertexes;
	}
	public void setVertexes(float[][] vertexes) {
		this.vertexes = vertexes;
	}
	public float[][] getUVs() {
		return uvs;
	}
	public void setUVs(float[][] uvs) {
		this.uvs = uvs;
	}
	public float[][] getNormals() {
		return normals;
	}
	public void setNormals(float[][] normals) {
		this.normals = normals;
	}
	public boolean isHasNormals() {
		return hasNormals;
	}
	public void setHasNormals(boolean hasNormals) {
		this.hasNormals = hasNormals;
	}
	public float[][] getColors() {
		return colors;
	}
	public void setColors(float[][] colors) {
		this.colors = colors;
	}
	public boolean isHasColors() {
		return hasColors;
	}
	public void setHasColors(boolean hasColors) {
		this.hasColors = hasColors;
	}
	public String getTextureName() {
		return textureName;
	}
	public void setTextureName(String textureName) {
		this.textureName = textureName;
	}
	public int getTexture() {
		return texture;
	}
	public void setTexture(int texture) {
		this.texture = texture;
	}
	public int getObjectID() {
		return objectID;
	}
	public void setObjectID(int objectID) {
		this.objectID = objectID;
	}
	public int getFrame() {
		return frame;
	}
	public void setFrame(int frame) {
		this.frame = frame;
	}
	public int getGeometry() {
		return geometry;
	}
	public void setGeometry(int geometry) {
		this.geometry = geometry;
	}
}
