package me.andre111.s5modeller.model;

public interface ModelVertexModifier {
	public float[] modifyVertex(float[] vertex, int index);
}
