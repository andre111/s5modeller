package me.andre111.s5modeller.model;

public class Quaternion {
	private float x;
	private float y;
	private float z;
	private float w;
	
	public Quaternion(float x, float y, float z, float w) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.w = w;
	}

	public Quaternion slerp(Quaternion end, float alpha) {
		final float d = this.x * end.x + this.y * end.y + this.z * end.z + this.w * end.w;
		float absDot = d < 0.f ? -d : d;

		// Set the first and second scale for the interpolation
		float scale0 = 1f - alpha;
		float scale1 = alpha;

		// Check if the angle between the 2 quaternions was big enough to
		// warrant such calculations
		if ((1 - absDot) > 0.1) {// Get the angle between the 2 quaternions,
			// and then store the sin() of that angle
			final float angle = (float)Math.acos(absDot);
			final float invSinTheta = 1f / (float)Math.sin(angle);

			// Calculate the scale for q1 and q2, according to the angle and
			// it's sine value
			scale0 = ((float)Math.sin((1f - alpha) * angle) * invSinTheta);
			scale1 = ((float)Math.sin((alpha * angle)) * invSinTheta);
		}

		if (d < 0.f) scale1 = -scale1;

		// Calculate the x, y, z and w values for the quaternion by using a
		// special form of linear interpolation for quaternions.
		x = (scale0 * x) + (scale1 * end.x);
		y = (scale0 * y) + (scale1 * end.y);
		z = (scale0 * z) + (scale1 * end.z);
		w = (scale0 * w) + (scale1 * end.w);

		// Return the interpolated quaternion
		return this;
	}
	
	public Quaternion normalize() {
		double n = Math.sqrt(x*x + y*y + z*z + w*w);
		x /= n;
		y /= n;
		z /= n;
		w /= n;
		return this;
	}
	
	public float[][] toMat4() {
		float[][] matrix = new float[4][4];
		matrix[0][0] = 1 - 2*y*y - 2*z*z;
		matrix[0][1] = 2*x*y + 2*z*w;
		matrix[0][2] = 2*x*z - 2*y*w;
		matrix[0][3] = 0;
		matrix[1][0] = 2*x*y - 2*z*w;
		matrix[1][1] = 1 - 2*x*x - 2*z*z;
		matrix[1][2] = 2*y*z + 2*x*w;
		matrix[1][3] = 0;
		matrix[2][0] = 2*x*z + 2*y*w;
		matrix[2][1] = 2*y*z - 2*x*w;
		matrix[2][2] = 1 - 2*x*x - 2*y*y;
		matrix[2][3] = 0;
		matrix[3][0] = 0;
		matrix[3][1] = 0;
		matrix[3][2] = 0;
		matrix[3][3] = 1;
		return matrix;
	}
}
