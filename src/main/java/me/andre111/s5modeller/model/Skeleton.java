package me.andre111.s5modeller.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.andre111.s5modeller.rwbs.RWSection;
import me.andre111.s5modeller.rwbs.data.RWByte;
import me.andre111.s5modeller.rwbs.data.RWByteArray2D;
import me.andre111.s5modeller.rwbs.data.RWFloat;
import me.andre111.s5modeller.rwbs.data.RWFloatArray2D;
import me.andre111.s5modeller.rwbs.data.RWInteger;
import me.andre111.s5modeller.rwbs.util.RWUtil;

public class Skeleton {
	private RWSection root;
	private RWSection frameList;
	private RWSection geometryList;
	
	private Bone rootBone;

	public Skeleton(RWSection rootSection) {
		root = rootSection;
		reload();
		
		if(rootBone!=null) {
			rootBone.debugPrint(true, false, "");
		}
	}
	
	public void reload() {
		List<Bone> boneLoading = new ArrayList<>();
		
		rootBone = null;
		
		frameList = RWUtil.getSection(root, 0x000E);
		geometryList = RWUtil.getSection(root, 0x001A);

		if(frameList!=null && geometryList!=null) {
			//TODO: This just assumes a single geometry
			RWSection skinPLG = RWUtil.getSection(RWUtil.getSection(RWUtil.getSection(geometryList, 0x000F), 0x0003), 0x116);
			if(skinPLG!=null) {
				System.out.println("Found skinned character!");
			
				RWSection frameListStruct = RWUtil.getSection(frameList, 0x0001);
				int frameCount = ((RWInteger) RWUtil.getData(frameListStruct, "frameCount")).getInteger();
				
				if(frameCount>1) {
					RWSection firstHAnimPLG = RWUtil.getSection(RWUtil.getSection(frameList, 0x0003, 1), 0x011E);
					
					if(firstHAnimPLG!=null) {
						int boneCount = ((RWInteger) RWUtil.getData(firstHAnimPLG, "boneCount")).getInteger();
						
						//TODO: Not every frame is necessarily a bone, only when they have a HAnim PLG? - FOr now assume they do (seems the work for settlers atleast)
						for(int i=0; i<boneCount; i++) {
							Bone bone = getBoneFromNumber(frameListStruct, frameCount, firstHAnimPLG, i, boneLoading);
							boneLoading.add(bone);
						}
						rootBone = boneLoading.get(0);
						
						
						//"bone usage"
						int[][] usedBones = ((RWByteArray2D) RWUtil.getData(skinPLG, "bonesUsed")).getArray();
						for(int i=0; i<usedBones.length; i++) {
							//System.out.println("Bone "+usedBones[i][0]+" is used!");
							boneLoading.get(usedBones[i][0]).setUsed();
						}
						//TODO: if usedBoneCount is 0, the engine ignores the used status and determines it itself
						
						
						//extract vertex weights
						int[][] vertexBoneIndices = ((RWByteArray2D) RWUtil.getData(skinPLG, "vertexBoneIndices")).getArray();
						float[][] vertexBoneWeights = ((RWFloatArray2D) RWUtil.getData(skinPLG, "vertexBoneWeights")).getArray();
						
						for(int i=0; i<vertexBoneIndices.length; i++) {
							for(int j=0; j<4; j++) {
								if(vertexBoneWeights[i][j]==0) { //TODO: unknown1 might also be a limit to how many bones a vertex can be mapped to
									break;
								}
								//System.out.println("Mapping vertex "+i+" to bone "+vertexBoneIndices[i][j]+" with weight "+vertexBoneWeights[i][j]);
								boneLoading.get(vertexBoneIndices[i][j]).mapVertex(i, vertexBoneWeights[i][j]);
							}
						}
						
						//sanity check for bone counts
						int skinBoneCount = ((RWByte) RWUtil.getData(skinPLG, "boneCount")).getByte();
						if(skinBoneCount!=boneLoading.size() || boneCount!=boneLoading.size()) {
							throw new IllegalStateException("Mismatched bonecount!");
						}
						System.out.println("Bone count is "+boneCount);
						
						//load inverse bone matrixes
						for(int i=0; i<boneCount; i++) {
							float[][] boneMatrix = ((RWFloatArray2D) RWUtil.getData(skinPLG, "boneMatrix", i)).getArray();
							
							boneLoading.get(i).setBoneMatrix(boneMatrix);
						}
					}
				}
			}
		}
	}
	
	private Bone getBoneFromNumber(RWSection frameListStruct, int frameCount, RWSection firstHAnimPLG, int number, List<Bone> bones) {
		int boneID = ((RWInteger) RWUtil.getData(firstHAnimPLG, "boneID", number)).getInteger();
		int flags = ((RWInteger) RWUtil.getData(firstHAnimPLG, "boneType", number)).getInteger();
		
		//find frame pos
		int pos = -1;
		for(int i=0; i<frameCount; i++) {
			RWSection hanim = RWUtil.getSection(RWUtil.getSection(frameList, 0x0003, i), 0x011E);
			if(hanim!=null) {
				int ownBoneID = ((RWInteger) RWUtil.getData(hanim, "ownBoneID")).getInteger();
				if(ownBoneID==boneID) {
					pos = i;
					break;
				}
			}
		}
		if(pos==-1) {
			throw new RuntimeException("Inconsistent bone numbering?");
		}
		
		//load parent bone
		int parentFrame = ((RWInteger) RWUtil.getData(frameListStruct, "parentFrame", pos)).getInteger();
		Bone parentBone = null;
		if(parentFrame>=0) {
			for(Bone bone : bones) {
				if(bone.frameIndex==parentFrame) {
					parentBone = bone;
					break;
				}
			}
			if(parentBone==null) {
				System.out.println("Warning: Found no parent bone to match parent frame!");
			}
		}
		
		//load location (linked to frame)
		RWFloatArray2D rotationMatrix = (RWFloatArray2D) RWUtil.getData(frameListStruct, "rotationMatrix", pos);
		RWFloat offsetX = (RWFloat) RWUtil.getData(frameListStruct, "coordinateOffsetX", pos);
		RWFloat offsetY = (RWFloat) RWUtil.getData(frameListStruct, "coordinateOffsetY", pos);
		RWFloat offsetZ = (RWFloat) RWUtil.getData(frameListStruct, "coordinateOffsetZ", pos);
		
		return new Bone(number, parentBone, boneID, flags, pos, rotationMatrix, offsetX, offsetY, offsetZ);
	}
	
	public Bone getRootBone() {
		return rootBone;
	}
	
	public static class Bone {
		private int boneNumber;
		private int boneID; //arbitrary number given in the model file, used for identifying bones
		private int flags;
		
		//hierachy
		private Bone parent = null;
		private List<Bone> children = new ArrayList<>();
		
		//vertex mapping
		private boolean used = false;
		private Map<Integer, Float> mappedVertices = new HashMap<>();
		
		//values from frame, describing relative location to parent
		private int frameIndex;
		private RWFloatArray2D frameRotationMatrix;
		private RWFloat frameOffsetX;
		private RWFloat frameOffsetY;
		private RWFloat frameOffsetZ;
		
		//IBM, used for transforming vertices into bone space
		private float[][] inverseBoneMatrix;
		
		//only used during runtime to transform vertices into pose
		//(contains absolute position of bone)
		private float[][] currentMatrix;
		
		private Bone(int index, Bone p, int id, int flag, int frameI, RWFloatArray2D rotMatrix, RWFloat offsetX, RWFloat offsetY, RWFloat offsetZ) {
			boneNumber = index;
			
			if(p!=null) {
				parent = p;
				parent.addChild(this);
			}
			
			boneID = id;
			flags = flag;
			
			frameIndex = frameI;
			frameRotationMatrix = rotMatrix;
			frameOffsetX = offsetX;
			frameOffsetY = offsetY;
			frameOffsetZ = offsetZ;
			
			//Initialize to identity
			currentMatrix = new float[4][4];
			currentMatrix[0][0] = 1;
			currentMatrix[1][1] = 1;
			currentMatrix[2][2] = 1;
			currentMatrix[3][3] = 1;
		}
		
		private void addChild(Bone child) {
			children.add(child);
		}
		
		private void setUsed() {
			used = true;
		}
		
		private void mapVertex(int vertexIndex, float weight) {
			if(!used) throw new IllegalStateException("Unused bone cannot have mapped vertices!");
			
			mappedVertices.put(vertexIndex, weight);
		}
		
		private void setBoneMatrix(float[][] matrix) {
			inverseBoneMatrix = matrix;
		}
		
		public int getBoneNumber() {
			return boneNumber;
		}
		
		public List<Bone> getChildren() {
			return children;
		}
		
		public float[][] getFrameRotationMatrix() {
			return frameRotationMatrix.getArray();
		}
		
		public float[] getFrameOffset() {
			return new float[] {
				frameOffsetX.getFloat(),
				frameOffsetY.getFloat(),
				frameOffsetZ.getFloat()
			};
		}
		
		public float[][] getInverseBoneMatrix() {
			return inverseBoneMatrix;
		}
		
		public float[][] getCurrentMatrix() {
			return currentMatrix;
		}
		
		public void setCurrentMatrix(float[][] matrix) {
			currentMatrix = matrix;
		}
		
		public boolean modifiesVertex(int vertexIndex) {
			return mappedVertices.containsKey(vertexIndex);
		}
		
		public float getVertexWeight(int vertexIndex) {
			return mappedVertices.get(vertexIndex);
		}
		
		@Override
		public String toString() {
			return "Bone "+boneNumber+" (ID: "+boneID+", Type: "+flags+", Used: "+used+", M.Vertices: "+mappedVertices.size()+")";
		}
		
		public void debugPrint(boolean first, boolean last, String insetSt) {
			//inset
			System.out.print(insetSt);
			
			if(!first) {
				if(last) {
					System.out.print("\u2514\u2500\u2500\u2500");
					insetSt = insetSt + "    ";
				} else {
					System.out.print("\u251C\u2500\u2500\u2500");
					insetSt = insetSt + "\u2502   ";
				}
			}
			
			//info
			System.out.println(toString());
			
			//children
			Bone previousChild = null;
			for(Bone child : children) {
				if(previousChild!=null) {
					previousChild.debugPrint(false, false, insetSt);
				}
				previousChild = child;
			}
			if(previousChild!=null) {
				previousChild.debugPrint(false, true, insetSt);
			}
		}
	}
}
