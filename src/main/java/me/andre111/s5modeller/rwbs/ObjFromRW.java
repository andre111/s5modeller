package me.andre111.s5modeller.rwbs;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;

import me.andre111.s5modeller.model.ModelPart;
import me.andre111.s5modeller.util.MatUtil;

public class ObjFromRW {
	private static int firstVertexID = 1;
	private static boolean flipV = true;

	public static void convertRW(ModelPart part, BufferedWriter writer) throws IOException {
		//for deduplication (because blender destroys some uvs when deduplicating)
		int[] vertexMapper = new int[part.getVertexCount()];
		ArrayList<VertexInfo> vertexInfos = new ArrayList<VertexInfo>();
		
		//write vertexes
		for(int i=0; i<part.getVertexCount(); i++) {
			VertexInfo vertex = new VertexInfo(part.getVertexes()[i]);
			if(vertexInfos.contains(vertex)) {
				vertexMapper[i] = vertexInfos.indexOf(vertex);
			} else {
				vertexMapper[i] = vertexInfos.size();
				vertexInfos.add(vertex);
				
				writer.write("v ");
				writer.write(part.getVertexes()[i][0]+" ");
				writer.write(part.getVertexes()[i][1]+" ");
				writer.write(part.getVertexes()[i][2]+"");
				writer.newLine();
			}
		}

		//write normals
		if(part.isHasNormals()) {
			for(int i=0; i<part.getVertexCount(); i++) {
				writer.write("vn ");
				writer.write(part.getNormals()[i][0]+" ");
				writer.write(part.getNormals()[i][1]+" ");
				writer.write(part.getNormals()[i][2]+"");
				writer.newLine();
			}
		}

		//write uvs
		for(int i=0; i<part.getVertexCount(); i++) {
			float u = part.getUVs()[i][0];
			float v = (flipV ? 1-part.getUVs()[i][1] : part.getUVs()[i][1]);

			/*while(u<0) u += 1;
				while(u>1) u -= 1;
				while(v<0) v += 1;
				while(v>1) v -= 1;*/

			writer.write("vt ");
			writer.write(u+" ");
			writer.write(v+"");
			writer.newLine();
		}

		//write faces
		int start = firstVertexID;
		int normalStart = firstVertexID;
		for(int i=0; i<part.getVertexCount(); i+=3) {
			writer.write("f ");

			if(part.isHasNormals()) {
				int v0 = vertexMapper[i+0];
				int v1 = vertexMapper[i+1];
				int v2 = vertexMapper[i+2];
				writer.write((v0+start)+"/"+(i+0+start)+"/"+(i+0+normalStart)+" ");
				writer.write((v1+start)+"/"+(i+1+start)+"/"+(i+1+normalStart)+" ");
				writer.write((v2+start)+"/"+(i+2+start)+"/"+(i+2+normalStart)+"");
				writer.newLine();
			} else {
				writer.write((i+0+start)+"/"+(i+0+start)+" ");
				writer.write((i+1+start)+"/"+(i+1+start)+" ");
				writer.write((i+2+start)+"/"+(i+2+start)+"");
				writer.newLine();
			}
		}
	}
	
	private static final class VertexInfo {
		private float[] pos;
		
		public VertexInfo(float[] pos) {
			this.pos = pos;
		}
		
		@Override
		public boolean equals(Object o) {
			if(o==null) return false;
			if(!(o instanceof VertexInfo)) return false;
			
			return MatUtil.checkSame(pos, ((VertexInfo) o).pos);
		}
	}
}
