package me.andre111.s5modeller.rwbs;

import java.io.IOException;

import me.andre111.s5modeller.util.EndianCorrectInputStream;
import me.andre111.s5modeller.util.EndianCorrectOutputStream;

public abstract class RWData {

	//returns number of bytes read
	public abstract int read(EndianCorrectInputStream dis) throws IOException;
	
	//returns number of bytes written
	public abstract int write(EndianCorrectOutputStream dos) throws IOException;
	
	public abstract String getName();
	
	public abstract String valueToString();
	
	//for gui editor
	public abstract boolean canEdit();
	
	public abstract boolean tryEdit(String newValue);
}
