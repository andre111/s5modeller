package me.andre111.s5modeller.rwbs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import me.andre111.s5modeller.rwbs.data.RWByte;
import me.andre111.s5modeller.rwbs.data.RWFloat;
import me.andre111.s5modeller.rwbs.data.RWFloatArray2D;
import me.andre111.s5modeller.rwbs.data.RWInteger;
import me.andre111.s5modeller.rwbs.data.RWIntegerArray2D;
import me.andre111.s5modeller.rwbs.data.RWShort;
import me.andre111.s5modeller.rwbs.data.RWShortArray2D;
import me.andre111.s5modeller.rwbs.data.RWString;
import me.andre111.s5modeller.rwbs.util.RWUtil;

public class RWFromObj {
	private static int firstVertexID = 1;
	private static float vertex_scale = 1;
	private static boolean flipV = true;
	
	//Blender Obj Export:
	//Enable: Apply Modifiers, Include Edges, Write Normals, Include UVs, Write Materials, Triangulate Faces, Objects as OBJ Objets
	//Forward: Y Forward
	//Up: Z Up
	//Scale: 100
	
	public static RWSection convertOBJ(File file, String textureName) {
		try(BufferedReader reader = new BufferedReader(new FileReader(file))) {
			ArrayList<VertexInfo> vertexes = new ArrayList<VertexInfo>();
			ArrayList<NormalInfo> normals = new ArrayList<NormalInfo>();
			ArrayList<UVInfo> uvs = new ArrayList<UVInfo>();
			
			ArrayList<FaceInfo> faces = new ArrayList<FaceInfo>();
			
			String line;
			while((line=reader.readLine()) != null) {
				String[] split = line.split(" ");
				
				switch(split[0]) {
				case "v": {
					VertexInfo info = new VertexInfo();
					info.pos[0] = Float.parseFloat(split[1]) * vertex_scale;
					info.pos[1] = Float.parseFloat(split[2]) * vertex_scale;
					info.pos[2] = Float.parseFloat(split[3]) * vertex_scale;
					vertexes.add(info);
					break;
				}
				case "vn": {
					NormalInfo info = new NormalInfo();
					info.normal[0] = Float.parseFloat(split[1]);
					info.normal[1] = Float.parseFloat(split[2]);
					info.normal[2] = Float.parseFloat(split[3]);
					normals.add(info);			
					break;
				}
				case "vt": {
					UVInfo info = new UVInfo();
					info.uv[0] = Float.parseFloat(split[1]);
					info.uv[1] = Float.parseFloat(split[2]);
					uvs.add(info);	
					break;
				}
				
				case "f": {
					FaceInfo info = new FaceInfo();
					for(int i=0; i<3; i++) {
						String faceVInfo = split[1+i];
						
						int vertex = -1;
						int normal = -1;
						int uv = -1;
						
						//Format v
						if(faceVInfo.indexOf("/")<0) {
							vertex = Integer.parseInt(faceVInfo);
						//Format: v//vn
						} else if(faceVInfo.indexOf("//")>=0) {
							String[] fvsplit = faceVInfo.split("//");
							vertex = Integer.parseInt(fvsplit[0]);
							normal = Integer.parseInt(fvsplit[1]);
						//Format: v/vt
						} else if(faceVInfo.indexOf("/")==faceVInfo.lastIndexOf("/")) {
							String[] fvsplit = faceVInfo.split("/");
							vertex = Integer.parseInt(fvsplit[0]);
							uv = Integer.parseInt(fvsplit[1]);
						//Format: v/vt/vn
						} else {
							String[] fvsplit = faceVInfo.split("/");
							vertex = Integer.parseInt(fvsplit[0]);
							uv = Integer.parseInt(fvsplit[1]);
							normal = Integer.parseInt(fvsplit[2]);
						}
						
						info.vertexes[i] = vertex-firstVertexID;
						info.normals[i] = normal-firstVertexID;
						info.uvs[i] = uv-firstVertexID;
						if(uv!=-1) info.hasUVs = true;
					}
					faces.add(info);
					break;
				}
				
				case "#": //comment
				case "mtllib": //ignore material(for now)
				case "usemtl": //ignore material(for now)
				case "s": //ignore shade(for now)
				default: {
					break;
				}
				}
			}
			
			
			RWSection converted = process(textureName, vertexes, normals, uvs, faces);

			
			return converted;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	private static RWSection process(String texname, ArrayList<VertexInfo> vertexes, ArrayList<NormalInfo> normals, ArrayList<UVInfo> uvs, ArrayList<FaceInfo> faces) {
		//create combined infos (needed because some verticies can be used multiple times with different uvs)
		float[] min = {Float.MAX_VALUE, Float.MAX_VALUE, Float.MAX_VALUE};
		float[] max = {-Float.MAX_VALUE, -Float.MAX_VALUE, -Float.MAX_VALUE};
		
		List<VertexNormalUVCombo> combinedVert = new ArrayList<VertexNormalUVCombo>();
		for(FaceInfo face : faces) {
			for(int i=0; i<3; i++) {
				VertexNormalUVCombo comb = new VertexNormalUVCombo();
				comb.vertex = vertexes.get(face.vertexes[i]);
				comb.normal = normals.get(face.normals[i]);
				if(face.hasUVs) {
					comb.hasUVs = true;
					comb.uv = uvs.get(face.uvs[i]);
				} else {
					comb.hasUVs = false;
				}
				
				//update min max
				for(int j=0; j<3; j++) {
					if(comb.vertex.pos[j]<min[j]) min[j] = comb.vertex.pos[j];
					if(comb.vertex.pos[j]>max[j]) max[j] = comb.vertex.pos[j];
				}
				
				int index = combinedVert.indexOf(comb);
				if(index==-1) {
					index = combinedVert.size();
					combinedVert.add(comb);
				}
				
				face.combined[i] = index;
			}
		}
		
		//calculate bounding sphere
		float boundingX = (min[0] + max[0]) / 2;
		float boundingY = (min[1] + max[1]) / 2;
		float boundingZ = (min[2] + max[2]) / 2;
		float boundingRadius = (float) Math.sqrt(((max[0] - min[0]) / 2) * ((max[0] - min[0]) / 2) + ((max[1] - min[1]) / 2) * ((max[1] - min[1]) / 2) + ((max[2] - min[2]) / 2) * ((max[2] - min[2]) / 2));
		
		//create NEW dff structure
		//TODO: Doesn't include any of the empty extensions (maybe needed for game to not crash)
		int version = 469893130; //TODO: These should be located somewhere globally
		RWSection clump = new RWSection(null, 0x0010, version);
		{
			RWSection clumpStruct = new RWSection(clump, 0x0001, version);
			{
				clumpStruct.addData("objectCount", new RWInteger().setInteger(1));
				clumpStruct.addData("unknown", new RWInteger().setInteger(0));
				clumpStruct.addData("unknown", new RWInteger().setInteger(0));
			}
			clump.addData("", clumpStruct);
			
			
			RWSection frameList = new RWSection(clump, 0x000E, version);
			{
				RWSection frameStruct = new RWSection(frameList, 0x0001, version);
				{
					//add parent frame
					frameStruct.addData("frameCount", new RWInteger().setInteger(2));
					RWFloatArray2D rot = new RWFloatArray2D(3, 3);
					rot.getArray()[0][0] = 1;
					rot.getArray()[0][1] = 0;
					rot.getArray()[0][2] = 0;
					
					rot.getArray()[1][0] = 0;
					rot.getArray()[1][1] = 1;
					rot.getArray()[1][2] = 0;
					
					rot.getArray()[2][0] = 0;
					rot.getArray()[2][1] = 0;
					rot.getArray()[2][2] = 1;
					frameStruct.addData("rotationMatrix", rot);
					frameStruct.addData("coordinateOffsetX", new RWFloat().setFloat(0));
					frameStruct.addData("coordinateOffsetY", new RWFloat().setFloat(0));
					frameStruct.addData("coordinateOffsetZ", new RWFloat().setFloat(0));

					//TODO: What to set here?
					frameStruct.addData("parentFrame", new RWInteger().setInteger(0xFFFFFFFF));
					frameStruct.addData("unknown", new RWInteger().setInteger(131075));
					
					//add frame
					RWFloatArray2D rot2 = new RWFloatArray2D(3, 3);
					rot2.getArray()[0][0] = 1;
					rot2.getArray()[0][1] = 0;
					rot2.getArray()[0][2] = 0;
					
					rot2.getArray()[1][0] = 0;
					rot2.getArray()[1][1] = 1;
					rot2.getArray()[1][2] = 0;
					
					rot2.getArray()[2][0] = 0;
					rot2.getArray()[2][1] = 0;
					rot2.getArray()[2][2] = 1;
					frameStruct.addData("rotationMatrix", rot2);
					frameStruct.addData("coordinateOffsetX", new RWFloat().setFloat(0));
					frameStruct.addData("coordinateOffsetY", new RWFloat().setFloat(0));
					frameStruct.addData("coordinateOffsetZ", new RWFloat().setFloat(0));

					//TODO: What to set here?
					frameStruct.addData("parentFrame", new RWInteger().setInteger(0));
					frameStruct.addData("unknown", new RWInteger().setInteger(0));
				}
				frameList.addData("", frameStruct);
				
				frameList.addData("", new RWSection(frameList, 0x0003, version));
				RWSection frameExt = new RWSection(frameList, 0x0003, version);
				{
					//load the HAnim stub that makes Buildings work (for some reason)
					RWSection sec = new RWSection(frameExt);
					RWUtil.loadRWSection(sec, RWFromObj.class.getResourceAsStream("/hanim_plg_stub.hex"));
					frameExt.addData("", sec);
				}
				frameList.addData("", frameExt);
			}
			clump.addData("", frameList);
			
			
			RWSection geomList = new RWSection(clump, 0x001A, version);
			{
				RWSection glistStruct = new RWSection(geomList, 0x0001, version);
				{
					glistStruct.addData("geometryCount", new RWInteger().setInteger(1));
				}
				geomList.addData("", glistStruct);
				
				RWSection geom = new RWSection(geomList, 0x000F, version);
				{
					RWSection geomStruct = new RWSection(geom, 0x0001, version);
					{
						geomStruct.addData("flag1", new RWByte().setByte(54)); //flags: Positions,Textured,Normals,Light set to true
						geomStruct.addData("flag2", new RWByte().setByte(1));
						
						geomStruct.addData("unknown", new RWByte().setByte(0));
						geomStruct.addData("unknown", new RWByte().setByte(0));
						
						geomStruct.addData("faceCount", new RWInteger().setInteger(faces.size()));
						geomStruct.addData("vertexCount", new RWInteger().setInteger(combinedVert.size()));
						geomStruct.addData("frameCount", new RWInteger().setInteger(1));
						
						RWFloatArray2D uv = new RWFloatArray2D(combinedVert.size(), 2);
						for(int i=0; i<combinedVert.size(); i++) {
							uv.getArray()[i][0] = combinedVert.get(i).uv.uv[0];
							uv.getArray()[i][1] = (flipV ? 1-combinedVert.get(i).uv.uv[1] : combinedVert.get(i).uv.uv[1]);
						}
						geomStruct.addData("uv", uv);
						
						RWShortArray2D face = new RWShortArray2D(faces.size(), 4);
						for(int i=0; i<faces.size(); i++) {
							face.getArray()[i][0] = (short) faces.get(i).combined[1];
							face.getArray()[i][1] = (short) faces.get(i).combined[0];
							face.getArray()[i][2] = 0;
							face.getArray()[i][3] = (short) faces.get(i).combined[2];
						}
						geomStruct.addData("faces", face);
						
						geomStruct.addData("boundingSphereX", new RWFloat().setFloat(boundingX));
						geomStruct.addData("boundingSphereY", new RWFloat().setFloat(boundingY));
						geomStruct.addData("boundingSphereZ", new RWFloat().setFloat(boundingZ));
						geomStruct.addData("boundingSphereRadius", new RWFloat().setFloat(boundingRadius));
						geomStruct.addData("unknown", new RWInteger().setInteger(1));
						geomStruct.addData("unknown", new RWInteger().setInteger(1));
						
						RWFloatArray2D vert = new RWFloatArray2D(combinedVert.size(), 3);
						for(int i=0; i<combinedVert.size(); i++) {
							vert.getArray()[i][0] = combinedVert.get(i).vertex.pos[0];
							vert.getArray()[i][1] = combinedVert.get(i).vertex.pos[1];
							vert.getArray()[i][2] = combinedVert.get(i).vertex.pos[2];
						}
						geomStruct.addData("vertexes", vert);
						
						RWFloatArray2D normal = new RWFloatArray2D(combinedVert.size(), 3);
						for(int i=0; i<combinedVert.size(); i++) {
							normal.getArray()[i][0] = combinedVert.get(i).normal.normal[0];
							normal.getArray()[i][1] = combinedVert.get(i).normal.normal[1];
							normal.getArray()[i][2] = combinedVert.get(i).normal.normal[2];
						}
						geomStruct.addData("normals", normal);
					}
					geom.addData("", geomStruct);
					
					RWSection matList = new RWSection(geom, 0x0008, version);
					{
						RWSection mlistStruct = new RWSection(matList, 0x0001, version);
						{
							mlistStruct.addData("materialCount", new RWInteger().setInteger(1));
							mlistStruct.addData("unknown", new RWInteger().setInteger(-1));
						}
						matList.addData("", mlistStruct);
						
						RWSection mat = new RWSection(matList, 0x0007, version);
						{
							RWSection matStruct = new RWSection(mat, 0x0001, version);
							{
								matStruct.addData("unknown", new RWInteger().setInteger(0));
								matStruct.addData("red", new RWByte().setByte(255));
								matStruct.addData("green", new RWByte().setByte(255));
								matStruct.addData("blue", new RWByte().setByte(255));
								matStruct.addData("alpha", new RWByte().setByte(255));
								matStruct.addData("unknown", new RWInteger().setInteger(0));
								matStruct.addData("textureCount", new RWInteger().setInteger(1));
								matStruct.addData("unknown", new RWFloat().setFloat(0.450f));
								matStruct.addData("unknown", new RWFloat().setFloat(1));
								matStruct.addData("unknown", new RWFloat().setFloat(1));
							}
							mat.addData("", matStruct);
							
							RWSection tex = new RWSection(mat, 0x0006, version);
							{
								RWSection texStruct = new RWSection(tex, 0x0001, version);
								{
									texStruct.addData("flags", new RWShort().setShort((short) 0x1106));
									texStruct.addData("unknown", new RWShort().setShort((short) 1));
								}
								tex.addData("", texStruct);
								
								RWSection diffuse = new RWSection(tex, 0x0002, version);
								diffuse.addData("string", new RWString(texname));
								tex.addData("", diffuse);
								
								RWSection mask = new RWSection(tex, 0x0002, version);
								mask.addData("string", new RWString(""));
								tex.addData("", mask);

								tex.addData("", new RWSection(tex, 0x0003, version));
							}
							mat.addData("", tex);
							
							RWSection matExt = new RWSection(mat, 0x0003, version);
							{
								RWSection unknown = new RWSection(matExt, 0x253F2FC, version);
								{
									unknown.addData("", new RWInteger().setInteger(0x3F800000));
									unknown.addData("", new RWInteger().setInteger(0x3F800000));
									unknown.addData("", new RWInteger().setInteger(0x3F800000));
									unknown.addData("", new RWInteger().setInteger(0x3F800000));
									unknown.addData("", new RWInteger().setInteger(0x3F000000));
									unknown.addData("", new RWInteger().setInteger(0x00000000));
								}
								matExt.addData("", unknown);
							}
							mat.addData("", matExt);
						}
						matList.addData("", mat);
					}
					geom.addData("", matList);
					
					RWSection ext = new RWSection(geom, 0x0003, version);
					{
						//TODO: Bin Mesh PLG
						RWSection binMesh = new RWSection(ext, 0x050E, version);
						{
							binMesh.addData("geometryTristrip", new RWInteger().setInteger(0));
							binMesh.addData("meshCount", new RWInteger().setInteger(1));
							binMesh.addData("indexCount", new RWInteger().setInteger(faces.size()*3));
							
							binMesh.addData("meshIndexCount", new RWInteger().setInteger(faces.size()*3));
							binMesh.addData("materialIndex", new RWInteger().setInteger(0));
							
							RWIntegerArray2D indices = new RWIntegerArray2D(faces.size()*3, 1);
							int pos = 0;
							for(int i=0; i<faces.size(); i++) {
								indices.getArray()[pos][0] = faces.get(i).combined[0];
								pos++;
								indices.getArray()[pos][0] = faces.get(i).combined[1];
								pos++;
								indices.getArray()[pos][0] = faces.get(i).combined[2];
								pos++;
							}
							binMesh.addData("indices", indices);
						}
						ext.addData("", binMesh);
						
						RWSection unknown2 = new RWSection(ext, 0x253F2FD, version);
						{
							unknown2.addData("", new RWInteger().setInteger(0x00000000));
						}
						ext.addData("", unknown2);
					}
					geom.addData("", ext);
				}
				geomList.addData("", geom);
			}	
			clump.addData("", geomList);
			
			RWSection atomic = new RWSection(clump, 0x0014, version);
			{
				RWSection atomicStruct = new RWSection(atomic, 0x0001, version);
				{
					atomicStruct.addData("frameIndex", new RWInteger().setInteger(1));
					atomicStruct.addData("geometryIndex", new RWInteger().setInteger(0));
					atomicStruct.addData("flags", new RWInteger().setInteger(5));
					atomicStruct.addData("unknown", new RWInteger().setInteger(0));
				}
				atomic.addData("", atomicStruct);
				
				RWSection atomicExt = new RWSection(atomic, 0x0003, version);
				{
					RWSection rtr = new RWSection(atomicExt, 0x001F, version);
					rtr.addData("", new RWInteger().setInteger(0x00000120));
					rtr.addData("", new RWInteger().setInteger(0x00000000));
					atomicExt.addData("", rtr);
					
					RWSection mateffplg = new RWSection(atomicExt, 0x0120, version);
					mateffplg.addData("", new RWInteger().setInteger(0x00000001));
					atomicExt.addData("", mateffplg);
				}
				atomic.addData("", atomicExt);
			}
			clump.addData("", atomic);

			clump.addData("", new RWSection(clump, 0x0003, version));
		}
		return clump;
	}
	
	private static class VertexInfo {
		public float[] pos = new float[3];
		
		@Override
		public boolean equals(Object o) {
			if(o == null) return false;
			if(!(o instanceof VertexInfo)) return false;
			
			VertexInfo other = (VertexInfo) o;
			return pos[0]==other.pos[0] && pos[1]==other.pos[1] && pos[2]==other.pos[2];
		}
	}
	
	private static class NormalInfo {
		public float[] normal = new float[3];
		
		@Override
		public boolean equals(Object o) {
			if(o == null) return false;
			if(!(o instanceof NormalInfo)) return false;
			
			NormalInfo other = (NormalInfo) o;
			return normal[0]==other.normal[0] && normal[1]==other.normal[1] && normal[2]==other.normal[2];
		}
	}
	
	private static class UVInfo {
		public float[] uv = new float[2];
		
		@Override
		public boolean equals(Object o) {
			if(o == null) return false;
			if(!(o instanceof UVInfo)) return false;
			
			UVInfo other = (UVInfo) o;
			return uv[0]==other.uv[0] && uv[1]==other.uv[1];
		}
	}
	
	private static class VertexNormalUVCombo {
		private VertexInfo vertex;
		private NormalInfo normal;
		private UVInfo uv;
		private boolean hasUVs;
		
		@Override
		public boolean equals(Object o) {
			if(o == null) return false;
			if(!(o instanceof VertexNormalUVCombo)) return false;
			
			VertexNormalUVCombo other = (VertexNormalUVCombo) o;
			if(!vertex.equals(other.vertex)) return false;
			if(!normal.equals(other.normal)) return false;
			if(hasUVs!=other.hasUVs) return false;
			if(hasUVs && !uv.equals(other.uv)) return false;
			
			return true;
		}
	}
	
	private static class FaceInfo {
		public int[] vertexes = new int[3];
		public int[] normals = new int[3];
		public int[] uvs = new int[3];
		public boolean hasUVs = false;
		
		public int[] combined = new int[3];
	}
}
