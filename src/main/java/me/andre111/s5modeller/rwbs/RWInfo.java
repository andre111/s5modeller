package me.andre111.s5modeller.rwbs;

public class RWInfo {
	private String name;
	private RWData data;
	
	public RWInfo(String n, RWData d) {
		name = n;
		data = d;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public RWData getData() {
		return data;
	}
	public void setData(RWData data) {
		this.data = data;
	}
}
