package me.andre111.s5modeller.rwbs;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import me.andre111.s5modeller.rwbs.data.RWByte;
import me.andre111.s5modeller.rwbs.data.RWByteArray2D;
import me.andre111.s5modeller.rwbs.data.RWFloat;
import me.andre111.s5modeller.rwbs.data.RWFloatArray2D;
import me.andre111.s5modeller.rwbs.data.RWInteger;
import me.andre111.s5modeller.rwbs.data.RWIntegerArray2D;
import me.andre111.s5modeller.rwbs.data.RWLeftoverBytes;
import me.andre111.s5modeller.rwbs.data.RWShort;
import me.andre111.s5modeller.rwbs.data.RWShortArray2D;
import me.andre111.s5modeller.rwbs.data.RWString;
import me.andre111.s5modeller.rwbs.util.RWUtil;
import me.andre111.s5modeller.util.EndianCorrectInputStream;
import me.andre111.s5modeller.util.EndianCorrectOutputStream;
import me.andre111.s5modeller.util.NullOutputStream;

public class RWSection extends RWData {
	private RWSection parent;
	
	private int id;
	private int version;
	
	private ArrayList<RWInfo> data = new ArrayList<RWInfo>();
	
	public RWSection(RWSection p) {
		parent = p;
	}
	
	public RWSection(RWSection p, int i, int v) {
		parent = p;
		id = i;
		version = v;
	}
	
	@Override
	public int read(EndianCorrectInputStream dis) throws IOException {
		//header data
		int current = 0;
		id = dis.readIntCorrect();
		int size = dis.readIntCorrect();
		version = dis.readIntCorrect();
		current += 3 * 4;
		
		int totalSize = size+3*4;
		
		//TODO - read section data based on id(from description files)
		//System.out.println("Found Section: "+getHexID());
		
		String infoString = getHexID();
		if(id==0x0001) {
			infoString = parent.getHexID() + "_struct";
		}
		RWSectionInfo info = RWSectionInfoList.getSectionInfo(infoString);
		
		data.clear();
		
		int repeatStart = 0;
		boolean repeat = false;
		int repeatCount = 0;
		int repeatMaxCount = -1;
		int command = 0;
		
		while(command<info.commands.size()) {
			String[] str = info.commands.get(command).str;
			
			switch(str[0]) {
			case "repeat": {
				repeatStart = command;
				repeat = true;
				repeatCount = 0;
				repeatMaxCount = -1;
				
				if(str.length==3 && str[1].equals("for")) {
					repeatMaxCount = getValue(str[2], size);
				}
				
				//skip to end of repeat(needed incase repeat is 0 times)
				if(current==totalSize) {
					while(command<info.commands.size() && !info.commands.get(command).str[0].equals("endrepeat")) {
						command++;
					}
					command--;
				}
				break;
			}
			case "endrepeat": {
				if(repeat && current<totalSize) {
					repeatCount++;
					if(repeatMaxCount==-1 || repeatCount<repeatMaxCount) {
						command = repeatStart;
					}
				}
				break;
			}
			case "if":
			case "if2": {
				//TODO - do if tests
				boolean istrue = false;
				
				int pos = 1;
				while(pos<str.length) {
					if(str[pos].equals("or")) {
						if(istrue==true) break;
						pos++;
					} else if(str[pos].equals("and")) {
						if(istrue==false) break;
						pos++;
					} else {
						int value = getValue(str[pos], size);
						int value2;
						String second = str[pos+1];
						
						if(second.equals("bit")) {
							int shift = getValue(str[pos+2], size);
							
							value = value & (1 << shift);
							if(value>0) {
								istrue = true;
							} else {
								istrue = false;
							}
							
							pos += 3;
						} else if(second.equals("not")) {
							value2 = getValue(str[pos+2], size);
							
							if(value!=value2) {
								istrue = true;
							} else {
								istrue = false;
							}
							
							pos += 3;
						} else {
							value2 = getValue(str[pos+1], size);
							
							if(value==value2) {
								istrue = true;
							} else {
								istrue = false;
							}
							
							pos += 2;
						}
					}
				}
				
				//skip if block, WARNING does not support nested ifs
				if(!istrue) {
					String endif = (str[0].equals("if") ? "endif" : "endif2");
					while(command<info.commands.size() && !info.commands.get(command).str[0].equals(endif)) {
						command++;
					}
				}
				break;
			}
			case "endif":
			case "endif2": {
				break;
			}
			
			//simple data types
			case "RWByte": {
				current += readRWData(str[1], new RWByte(), dis);
				break;
			}
			case "RWShort": {
				current += readRWData(str[1], new RWShort(), dis);
				break;
			}
			case "RWInteger": {
				current += readRWData(str[1], new RWInteger(), dis);
				break;
			}
			case "RWFloat": {
				current += readRWData(str[1], new RWFloat(), dis);
				break;
			}
			case "RWString": {
				int strsize = getValue(str[2], size);
				
				current += readRWData(str[1], new RWString(strsize), dis);
				break;
			}
			
			//TODO - arrays are they done?
			case "RWByteArray2D": {
				int i = getValue(str[2], size);
				int j = getValue(str[3], size);
				
				current += readRWData(str[1], new RWByteArray2D(i, j), dis);
				break;
			}
			case "RWShortArray2D": {
				int i = getValue(str[2], size);
				int j = getValue(str[3], size);
				
				current += readRWData(str[1], new RWShortArray2D(i, j), dis);
				break;
			}
			case "RWIntegerArray2D": {
				int i = getValue(str[2], size);
				int j = getValue(str[3], size);
				
				current += readRWData(str[1], new RWIntegerArray2D(i, j), dis);
				break;
			}
			case "RWFloatArray2D": {
				int i = getValue(str[2], size);
				int j = getValue(str[3], size);
				
				current += readRWData(str[1], new RWFloatArray2D(i, j), dis);
				break;
			}
			
			case "RWSection": {
				//TODO - needs a way to get name
				current += readRWData(str.length>=2 ? str[1] : "", new RWSection(this), dis);
				break;
			}
			
			default: {
				System.out.println("WARNING: Found unknown command: "+str[0]);
				break;
			}
			}
			
			
			command++;
		}
		
		
		//read remaining data
		if(current<totalSize) {
			System.out.println("Found "+(totalSize-current)+" leftover bytes in "+getHexID()+"!");
			current += readRWData("left-over", new RWLeftoverBytes(totalSize-current), dis);
		} else if (current>totalSize) {
			System.out.println("WARNING: Read "+(current-totalSize)+" to many bytes in "+getHexID()+"! (Size should be: "+(totalSize-3*4)+")");
			if(id==0x0001) System.out.println("Parent: "+this.getParent().getHexID());
		}
		
		return current;
	}
	
	private int readRWData(String name, RWData rwdata, EndianCorrectInputStream dis) throws IOException {
		int count = rwdata.read(dis);
		
		//TODO - Maybe: If section get name from id
		/*if(name.equals("") && rwdata instanceof RWSection) {
			name = RWSectionInfoList.getSectionName(((RWSection) rwdata).getHexID());
		}*/
		
		data.add(new RWInfo(name, rwdata));
		
		return count;
	}
	
	private int getValue(String st, int size) {
		int i = 0;
		
		if(st.equals("size")) {
			return size;
		}
		
		try {
			//try reading as int
			i = Integer.parseInt(st, 10);
		} catch(NumberFormatException e) {
			//else get from allready read value
			List<RWInfo> dataList = data;
			
			//navigate to fitting section
			if(st.contains("->") && st.contains(".")) {
				String[] split = st.split("->");
				
				//extract name
				st = split[split.length-1].split("\\.")[1];
				split[split.length-1] = split[split.length-1].split("\\.")[0];
				
				//navigate structure
				RWSection currentSection = this;
				for(int s=0; s<split.length; s++) {
					if(split[s].equals("parent")) {
						currentSection = currentSection.getParent();
					} else if(split[s].startsWith("0x")) {
						currentSection = RWUtil.getSection(currentSection, Integer.parseInt(split[s].substring(2), 16));
					}
				}
				dataList = currentSection.getData();
			}
			
			//extract value
			for(RWInfo info : dataList) {
				if(info.getName().equals(st)) {
					RWData rwdata = info.getData();
					
					if(rwdata instanceof RWByte) {
						i = ((RWByte) rwdata).getByte();
					} else if(rwdata instanceof RWShort) {
						i = ((RWShort) rwdata).getShort();
					} else if(rwdata instanceof RWInteger) {
						i = ((RWInteger) rwdata).getInteger();
					}
					
					//NO BREAK, to get LAST occurence of name(for repeat loops)
					//break;
				}
			}
		}
		
		return i;
	}
	
	@Override
	public int write(EndianCorrectOutputStream dos) throws IOException {
		//header data
		int current = 0;
		dos.writeIntCorrect(id);
		dos.writeIntCorrect(calculateDataSize()); //TODO - size calculation
		dos.writeIntCorrect(version);
		current += 3 * 4;
		
		//write section data
		for(RWInfo info : data) {
			current += info.getData().write(dos);
		}
		
		return current;
	}
	
	//TODO - use sparingly because this uses many resources right now
	private int calculateDataSize() throws IOException {
		EndianCorrectOutputStream dos = new EndianCorrectOutputStream(new NullOutputStream(), false);
		int count = 0;
		
		for(RWInfo info : data) {
			count += info.getData().write(dos);
		}
		
		return count;
	}
	
	public String getHexID() {
		String st = Integer.toHexString(id).toUpperCase();
		int length = 4;
		if(id>65535) length = 8;
		
		while(st.length()>length) {
			st = st.substring(1);
		}
		while(st.length()<length) {
			st = "0" + st;
		}
		
		st = "0x" + st;
		
		return st;
	}
	
	public void addData(String name, RWData rwdata) {
		data.add(new RWInfo(name, rwdata));
	}
	
	//TODO: This will only delete the same object not one that equals it!
	public void removeData(RWData rwdata) {
		for(int i=0; i<data.size(); i++) {
			if(data.get(i).getData()==rwdata) {
				data.remove(i);
				return;
			}
		}
	}
	
	public String getName() {
		return RWSectionInfoList.getSectionName(getHexID());
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append(getName());
		sb.append(" (");
		sb.append(getHexID());
		sb.append(" - ");
		try {
			sb.append(calculateDataSize());
		} catch (IOException e) {
		}
		sb.append(" bytes)");
		
		return sb.toString();
	}
	
	public void printInfo(boolean first, boolean last, String insetSt) {
		//inset
		System.out.print(insetSt);
		
		if(!first) {
			if(last) {
				System.out.print("\u2514\u2500\u2500\u2500");
				insetSt = insetSt + "    ";
			} else {
				System.out.print("\u251C\u2500\u2500\u2500");
				insetSt = insetSt + "\u2502   ";
			}
		}
		
		//info
		System.out.println(toString());
		
		//children
		RWSection previousSection = null;
		for(RWInfo info : data) {
			if(info.getData() instanceof RWSection) {
				if(previousSection!=null) {
					previousSection.printInfo(false, false, insetSt);
				}
				previousSection = ((RWSection) info.getData());
			}
		}
		if(previousSection!=null) {
			previousSection.printInfo(false, true, insetSt);
		}
	}
	
	public int getID() {
		return id;
	}
	
	public RWSection getParent() {
		return parent;
	}
	
	public ArrayList<RWInfo> getData() {
		return data;
	}

	@Override
	public String valueToString() {
		return "skipped";
	}

	@Override
	public boolean canEdit() {
		return false;
	}

	@Override
	public boolean tryEdit(String newValue) {
		return false;
	}
}
