package me.andre111.s5modeller.rwbs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

import me.andre111.s5modeller.rwbs.RWSectionInfo.RWSICommand;

public class RWSectionInfoList {
	private static HashMap<String, RWSectionInfo> infos = new HashMap<String, RWSectionInfo>();
	private static HashMap<String, String> sectionNames = new HashMap<String, String>();
	
	public static boolean hasSectionDescription(RWSection section) {
		String id = section.getHexID();
		//special case -> struct
		if(section.getID()==0x0001 && section.getParent()!=null) {
			id = section.getParent().getHexID()+"_struct";
		}
		
		File file = new File("./data/sections/"+id+".txt");
		return file.exists();
	}
	
	public static RWSectionInfo getSectionInfo(String id) {
		if(infos.containsKey(id)) {
			return infos.get(id);
		}
		
		RWSectionInfo info = new RWSectionInfo();
		infos.put(id, info);
		
		File file = new File("./data/sections/"+id+".txt");
		if(file.exists()) {
			try(BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)))) {
				String line = "";
				while((line = reader.readLine()) != null) {
					//remove leading spaces
					while(line.startsWith(" ") || line.startsWith("\t")) {
						line = line.substring(1);
					}
					
					//skip empty and comment lines
					if(line.equals("") || line.startsWith("//")) continue;
					
					RWSICommand command = new RWSICommand();
					command.str = line.split(" ");
					info.commands.add(command);
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return infos.get(id);
	}
	
	public static String getSectionName(String hexID) {
		//initial name file loading
		if(sectionNames.isEmpty()) {
			File file = new File("./data/sections/names.txt");
			if(file.exists()) {
				try(BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)))) {
					String line = "";
					while((line = reader.readLine()) != null) {
						String[] split = line.split("=");
						sectionNames.put(split[0], split[1]);
					}
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		if(sectionNames.containsKey(hexID)) {
			return sectionNames.get(hexID);
		} else {
			return "Unknown";
		}
	}
}
