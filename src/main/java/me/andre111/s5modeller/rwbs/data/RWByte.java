package me.andre111.s5modeller.rwbs.data;

import java.io.IOException;

import me.andre111.s5modeller.rwbs.RWData;
import me.andre111.s5modeller.util.EndianCorrectInputStream;
import me.andre111.s5modeller.util.EndianCorrectOutputStream;

public class RWByte extends RWData {
	//using int, because bytes in RW are usually unsigned
	private int data;

	@Override
	public int read(EndianCorrectInputStream dis) throws IOException {
		data = dis.readUnsignedByte();
		return 1;
	}

	@Override
	public int write(EndianCorrectOutputStream dos) throws IOException {
		dos.writeByte(data);
		return 1;
	}
	
	public int getByte() {
		return data;
	}
	
	public RWByte setByte(int b) {
		data = b;
		return this;
	}

	@Override
	public String getName() {
		return "Byte";
	}

	@Override
	public String valueToString() {
		return ""+data;
	}
	
	@Override
	public boolean canEdit() {
		return true;
	}

	@Override
	public boolean tryEdit(String newValue) {
		try {
			data = Integer.parseInt(newValue);
			//TODO: Check if value is in range
			return true;
		} catch(NumberFormatException e) {
		}
		return false;
	}
}
