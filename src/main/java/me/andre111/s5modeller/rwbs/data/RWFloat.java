package me.andre111.s5modeller.rwbs.data;

import java.io.IOException;

import me.andre111.s5modeller.rwbs.RWData;
import me.andre111.s5modeller.util.EndianCorrectInputStream;
import me.andre111.s5modeller.util.EndianCorrectOutputStream;

public class RWFloat extends RWData {
	float data;

	@Override
	public int read(EndianCorrectInputStream dis) throws IOException {
		data = dis.readFloatCorrect();
		return 4;
	}

	@Override
	public int write(EndianCorrectOutputStream dos) throws IOException {
		dos.writeFloatCorrect(data);
		return 4;
	}

	public float getFloat() {
		return data;
	}
	
	public RWFloat setFloat(float f) {
		data = f;
		return this;
	}

	@Override
	public String getName() {
		return "Float";
	}

	@Override
	public String valueToString() {
		return ""+data;
	}
	
	@Override
	public boolean canEdit() {
		return true;
	}

	@Override
	public boolean tryEdit(String newValue) {
		try {
			data = Float.parseFloat(newValue);
			return true;
		} catch(NumberFormatException e) {
		}
		return false;
	}
}
