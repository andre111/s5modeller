package me.andre111.s5modeller.rwbs.data;

import java.io.IOException;

import me.andre111.s5modeller.rwbs.RWData;
import me.andre111.s5modeller.util.EndianCorrectInputStream;
import me.andre111.s5modeller.util.EndianCorrectOutputStream;

public class RWFloatArray2D extends RWData {
	private int size1;
	private int size2;
	
	private float[][] data;
	
	public RWFloatArray2D(int s1, int s2) {
		size1 = s1;
		size2 = s2;
		
		data = new float[s1][s2];
	}

	@Override
	public int read(EndianCorrectInputStream dis) throws IOException {
		for(int i=0; i<size1; i++) {
			data[i] = new float[size2];
			for(int j=0; j<size2; j++) {
				data[i][j] = dis.readFloatCorrect();
			}
		}
		return size1*size2*4;
	}

	@Override
	public int write(EndianCorrectOutputStream dos) throws IOException {
		for(int i=0; i<size1; i++) {
			for(int j=0; j<size2; j++) {
				dos.writeFloatCorrect(data[i][j]);
			}
		}
		return size1*size2*4;
	}

	public float[][] getArray() {
		return data;
	}
	
	@Override
	public String getName() {
		return size1+"x"+size2+" Float Array";
	}

	@Override
	public String valueToString() {
		return (size1*size2)+" floats skipped";
	}
	
	@Override
	public boolean canEdit() {
		return false;
	}

	@Override
	public boolean tryEdit(String newValue) {
		return false;
	}
}
