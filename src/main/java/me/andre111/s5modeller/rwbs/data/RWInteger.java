package me.andre111.s5modeller.rwbs.data;

import java.io.IOException;

import me.andre111.s5modeller.rwbs.RWData;
import me.andre111.s5modeller.util.EndianCorrectInputStream;
import me.andre111.s5modeller.util.EndianCorrectOutputStream;

public class RWInteger extends RWData {
	private int data;
	
	@Override
	public int read(EndianCorrectInputStream dis) throws IOException {
		data = dis.readIntCorrect();
		return 4;
	}

	@Override
	public int write(EndianCorrectOutputStream dos) throws IOException {
		dos.writeIntCorrect(data);
		return 4;
	}
	
	public int getInteger() {
		return data;
	}
	
	public RWInteger setInteger(int i) {
		data = i;
		return this;
	}

	@Override
	public String getName() {
		return "Integer";
	}

	@Override
	public String valueToString() {
		return ""+data;
	}
	
	@Override
	public boolean canEdit() {
		return true;
	}

	@Override
	public boolean tryEdit(String newValue) {
		try {
			data = Integer.parseInt(newValue);
			return true;
		} catch(NumberFormatException e) {
		}
		return false;
	}
}
