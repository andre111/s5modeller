package me.andre111.s5modeller.rwbs.data;

import java.io.IOException;

import me.andre111.s5modeller.rwbs.RWData;
import me.andre111.s5modeller.util.EndianCorrectInputStream;
import me.andre111.s5modeller.util.EndianCorrectOutputStream;

public class RWLeftoverBytes extends RWData {
	private int count;
	private int[] data;
	
	public RWLeftoverBytes(int count) {
		this.count = count;
		data = new int[count];
	}

	@Override
	public int read(EndianCorrectInputStream dis) throws IOException {
		for(int i=0; i<count; i++) {
			data[i] = dis.readUnsignedByte();
		}
		
		return count;
	}

	@Override
	public int write(EndianCorrectOutputStream dos) throws IOException {
		for(int i=0; i<count; i++) {
			dos.writeByte(data[i]);
		}
		
		return count;
	}
	
	@Override
	public String getName() {
		return "Unknown Data";
	}

	@Override
	public String valueToString() {
		return count+" bytes skipped";
	}
	
	@Override
	public boolean canEdit() {
		return false;
	}

	@Override
	public boolean tryEdit(String newValue) {
		return false;
	}
}
