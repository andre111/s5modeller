package me.andre111.s5modeller.rwbs.data;

import java.io.IOException;

import me.andre111.s5modeller.rwbs.RWData;
import me.andre111.s5modeller.util.EndianCorrectInputStream;
import me.andre111.s5modeller.util.EndianCorrectOutputStream;

public class RWShort extends RWData {
	private short data;

	@Override
	public int read(EndianCorrectInputStream dis) throws IOException {
		data = dis.readShortCorrect();
		return 2;
	}

	@Override
	public int write(EndianCorrectOutputStream dos) throws IOException {
		dos.writeShortCorrect(data);
		return 2;
	}
	
	public short getShort() {
		return data;
	}
	
	public RWShort setShort(short s) {
		data = s;
		return this;
	}

	@Override
	public String getName() {
		return "Short";
	}

	@Override
	public String valueToString() {
		return ""+data;
	}
	
	@Override
	public boolean canEdit() {
		return true;
	}

	@Override
	public boolean tryEdit(String newValue) {
		try {
			data = Short.parseShort(newValue);
			//TODO: Check if value is in range
			return true;
		} catch(NumberFormatException e) {
		}
		return false;
	}
}
