package me.andre111.s5modeller.rwbs.data;

import java.io.IOException;
import java.util.ArrayList;

import me.andre111.s5modeller.rwbs.RWData;
import me.andre111.s5modeller.util.EndianCorrectInputStream;
import me.andre111.s5modeller.util.EndianCorrectOutputStream;

public class RWString extends RWData {
	//TODO - string is actually a section and needs size, ...
	//it is read correctly here, but section can contain additional data
	private String data;
	
	private int size;
	private ArrayList<Integer> leftoverBytes = new ArrayList<Integer>();

	public RWString(int s) {
		size = s;
	}
	
	public RWString(String string) {
		data = string;
		size = data.length();
		leftoverBytes.add(0);
		if(string.equals("")) {
			leftoverBytes.add(0);
			leftoverBytes.add(0);
			leftoverBytes.add(0);
		}
	}
	
	@Override
	public int read(EndianCorrectInputStream dis) throws IOException {
		int count = 0;
		boolean append = true;;
		
		StringBuilder sb = new StringBuilder();
		for(int i=0; i<size; i++) {
			int c = dis.readUnsignedByte();
			if(c==0) append = false;
			
			if(append) {
				sb.append((char) c);
			} else {
				leftoverBytes.add(c);
			}
			count++;
		}
		data = sb.toString();
		
		return count;
	}

	@Override
	public int write(EndianCorrectOutputStream dos) throws IOException {
		int i = 0;
		for(i=0; i<data.length(); i++) {
			dos.writeByte(data.charAt(i));
		}
		
		for(int lo : leftoverBytes) {
			dos.writeByte(lo);
		}
		
		return data.length()+leftoverBytes.size();
	}

	public String getString() {
		return data;
	}

	@Override
	public String getName() {
		return "String";
	}

	@Override
	public String valueToString() {
		return data;
	}
	
	@Override
	public boolean canEdit() {
		return true;
	}

	@Override
	public boolean tryEdit(String newValue) {
		data = newValue;
		leftoverBytes.clear();
		leftoverBytes.add(0);
		//dff files always seem to have atleast 4 bytes in a string
		while(data.length()+leftoverBytes.size()<4) {
			leftoverBytes.add(0);
		}
		
		return true;
	}
}
