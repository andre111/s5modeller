package me.andre111.s5modeller.rwbs.util;

import me.andre111.s5modeller.rwbs.RWSection;
import me.andre111.s5modeller.rwbs.data.RWByte;
import me.andre111.s5modeller.rwbs.data.RWFloatArray2D;
import me.andre111.s5modeller.util.MatUtil;

public class RWHacks {
	public static void copyUVasUV2(RWSection target, RWSection source) {
		RWSection geometryListTarget = RWUtil.getSection(target, 0x001A);
		RWSection geometryListSource = RWUtil.getSection(source, 0x001A);
		int g = 0;
		while(RWUtil.getSection(geometryListTarget, 0x000F, g)!=null) {
			RWSection geomStructTarget = RWUtil.getSection(RWUtil.getSection(geometryListTarget, 0x000F, g), 0x0001);
			RWSection geomStructSource = RWUtil.getSection(RWUtil.getSection(geometryListSource, 0x000F, g), 0x0001);

			//set uv2 flag
			RWByte flag = (RWByte) RWUtil.getData(geomStructTarget, "flag1");
			flag.setByte(flag.getByte() | (1<<7));

			RWFloatArray2D vertexTarget = (RWFloatArray2D) RWUtil.getData(geomStructTarget, "vertexes");
			RWFloatArray2D vertexSource = (RWFloatArray2D) RWUtil.getData(geomStructSource, "vertexes");
			RWFloatArray2D normalsTarget = (RWFloatArray2D) RWUtil.getData(geomStructTarget, "normals");
			RWFloatArray2D normalsSource = (RWFloatArray2D) RWUtil.getData(geomStructSource, "normals");
			RWFloatArray2D uvsTarget = new RWFloatArray2D(vertexTarget.getArray().length, 2);
			RWFloatArray2D uvsSource = (RWFloatArray2D) RWUtil.getData(geomStructSource, "uv");

			for(int i=0; i<vertexTarget.getArray().length; i++) {
				//find fitting array
				int fit = -1;
				for(int j=0; i<vertexSource.getArray().length; j++) {
					if(MatUtil.checkSame(vertexTarget.getArray()[i], vertexSource.getArray()[j]) && MatUtil.checkSame(normalsTarget.getArray()[i], normalsSource.getArray()[j])) {
						fit = j;
						break;
					}
				}

				if(fit==-1) {
					System.out.println("Warning: found non fitting vertex!");
				} else {
					uvsTarget.getArray()[i] = uvsSource.getArray()[fit];
				}
			}

			//add uv2
			RWUtil.insertData(geomStructTarget, "uv2", uvsTarget, "uv");

			g++;
		}
	}
}
