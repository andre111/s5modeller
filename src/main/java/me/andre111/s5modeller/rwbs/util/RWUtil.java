package me.andre111.s5modeller.rwbs.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import me.andre111.s5modeller.rwbs.RWData;
import me.andre111.s5modeller.rwbs.RWInfo;
import me.andre111.s5modeller.rwbs.RWSection;
import me.andre111.s5modeller.util.EndianCorrectInputStream;
import me.andre111.s5modeller.util.EndianCorrectOutputStream;

public class RWUtil {
	public static RWSection getSection(RWSection section, int id) {
		return getSection(section, id, 0);
	}
	public static RWSection getSection(RWSection section, int id, int pos) {
		for(RWInfo data : section.getData()) {
			if(data.getData() instanceof RWSection) {
				RWSection child = (RWSection) data.getData();

				if(child.getID()==id) {
					if(pos==0) {
						return child;
					}
					pos--;
				}
			}
		}

		return null;
	}

	public static RWData getData(RWSection section, String name) {
		return getData(section, name, 0);
	}
	public static RWData getData(RWSection section, String name, int pos) {
		for(RWInfo data : section.getData()) {
			if(data.getName().equals(name)) {
				if(pos==0) {
					return data.getData();
				}
				pos--;
			}
		}

		return null;
	}

	public static void insertData(RWSection section, String name, RWData data, String after) {
		RWInfo info = new RWInfo(name, data);
		for(int i=0; i<section.getData().size(); i++) {
			if(section.getData().get(i).getName().equals(after)) {
				section.getData().add(i+1, info);
				break;
			}
		}
	}

	//load and save
	public static RWSection loadRWSection(File file) {
		//TODO - this should use the RWBinary... class
		RWSection section = new RWSection(null);
		if(loadRWSection(section, file)) {
			return section;
		} else {
			return null;
		}
	}

	public static boolean loadRWSection(RWSection section, File file) {
		try(FileInputStream input = new FileInputStream(file)) {
			return loadRWSection(section, input);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return false;
	}
	public static boolean loadRWSection(RWSection section, InputStream input) {
		//should always produce the exact binary file
		try(EndianCorrectInputStream dis = new EndianCorrectInputStream(input, false)) {
			section.read(dis);

			return true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return false;
	}

	public static boolean saveRWSection(RWSection sec, File file) {
		//should always produce the exact binary file
		try(EndianCorrectOutputStream dos = new EndianCorrectOutputStream(new FileOutputStream(file), false)) {
			sec.write(dos);
			
			return true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return false;
	}
}
