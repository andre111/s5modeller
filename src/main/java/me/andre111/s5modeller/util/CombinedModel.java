package me.andre111.s5modeller.util;

import me.andre111.s5modeller.rwbs.RWSection;
import me.andre111.s5modeller.rwbs.data.RWFloat;
import me.andre111.s5modeller.rwbs.data.RWFloatArray2D;
import me.andre111.s5modeller.rwbs.data.RWInteger;
import me.andre111.s5modeller.rwbs.util.RWUtil;

public class CombinedModel {
	private RWSection clump;
	private RWSection frameList;
	private RWSection frameStruct;
	private RWSection geomList;
	
	private RWInteger objectCount;
	private RWInteger frameCount;
	private RWInteger geometryCount;
	
	public CombinedModel() {
		int version = 469893130;
		clump = new RWSection(null, 0x0010, version);
		{
			RWSection clumpStruct = new RWSection(clump, 0x0001, version);
			{
				objectCount = new RWInteger().setInteger(0);
				clumpStruct.addData("objectCount", objectCount);
				clumpStruct.addData("unknown", new RWInteger().setInteger(0));
				clumpStruct.addData("unknown", new RWInteger().setInteger(0));
			}
			clump.addData("", clumpStruct);
			
			frameList = new RWSection(clump, 0x000E, version);
			{
				frameStruct = new RWSection(frameList, 0x0001, version);
				{
					frameCount = new RWInteger().setInteger(1);
					frameStruct.addData("frameCount", frameCount);
					//add parent frame
					RWFloatArray2D rot = new RWFloatArray2D(3, 3);
					rot.getArray()[0][0] = 1;
					rot.getArray()[0][1] = 0;
					rot.getArray()[0][2] = 0;
					
					rot.getArray()[1][0] = 0;
					rot.getArray()[1][1] = 1;
					rot.getArray()[1][2] = 0;
					
					rot.getArray()[2][0] = 0;
					rot.getArray()[2][1] = 0;
					rot.getArray()[2][2] = 1;
					frameStruct.addData("rotationMatrix", rot);
					frameStruct.addData("coordinateOffsetX", new RWFloat().setFloat(0));
					frameStruct.addData("coordinateOffsetY", new RWFloat().setFloat(0));
					frameStruct.addData("coordinateOffsetZ", new RWFloat().setFloat(0));

					//TODO: What to set here?
					frameStruct.addData("parentFrame", new RWInteger().setInteger(0xFFFFFFFF));
					frameStruct.addData("unknown", new RWInteger().setInteger(131075));
				}
				frameList.addData("", frameStruct);
				
				//add parent frame
				frameList.addData("", new RWSection(frameList, 0x0003, version));
			}
			clump.addData("", frameList);
			
			geomList = new RWSection(clump, 0x001A, version);
			{
				RWSection glistStruct = new RWSection(geomList, 0x0001, version);
				{
					geometryCount = new RWInteger().setInteger(0);
					glistStruct.addData("geometryCount", geometryCount);
				}
				geomList.addData("", glistStruct);
			}
			clump.addData("", geomList);
		}
	}
	
	public RWSection finishClump() {
		//add closing extension
		int version = 469893130;
		clump.addData("", new RWSection(clump, 0x0003, version));
		return clump;
	}
	
	public void addGeometryFrom(RWSection sec, float x, float y, float z, float rot, float scale) {
		addGeometryFrom(sec, x, y, z, rot, scale, scale, scale);
	}
	public void addGeometryFrom(RWSection sec, float x, float y, float z, float rot, float scaleX, float scaleY, float scaleZ) {
		//loop through all atomics
		int pos = 0;
		while(RWUtil.getSection(sec, 0x0014, pos)!=null) {
			RWSection atomic = RWUtil.getSection(sec, 0x0014, pos);
			RWSection atomicStruct = RWUtil.getSection(atomic, 0x0001);
			int frame = ((RWInteger) RWUtil.getData(atomicStruct, "frameIndex")).getInteger();
			int geom = ((RWInteger) RWUtil.getData(atomicStruct, "geometryIndex")).getInteger();
			
			//increase object count
			objectCount.setInteger(objectCount.getInteger()+1);
			
			//find number for new frame and geometry
			int newFrame = frameCount.getInteger();
			frameCount.setInteger(frameCount.getInteger()+1);
			
			int newGeom = geometryCount.getInteger();
			geometryCount.setInteger(geometryCount.getInteger()+1);
			
			
			//adjust and copy atomic
			((RWInteger) RWUtil.getData(atomicStruct, "frameIndex")).setInteger(newFrame);
			((RWInteger) RWUtil.getData(atomicStruct, "geometryIndex")).setInteger(newGeom);
			clump.addData("", atomic);
			
			//find and copy corresponding framedata 
			//(TODO: frame may be in a tree structure (parentFrame) which has multiple layered translations)
			RWSection oldFrameList = RWUtil.getSection(sec, 0x000E);
			RWSection oldFrameListStruct = RWUtil.getSection(oldFrameList, 0x0001);
			
			RWFloatArray2D rotMat = (RWFloatArray2D) RWUtil.getData(oldFrameListStruct, "rotationMatrix", frame);
			rotMat = MatUtil.multiply(rotMat, MatUtil.getRotMatrix(rot, "Z"));
			rotMat = MatUtil.multiply(rotMat, MatUtil.getScaleMatrix(scaleX, scaleY, scaleZ));
			frameStruct.addData("rotationMatrix", rotMat);
			
			RWFloat offX = (RWFloat) RWUtil.getData(oldFrameListStruct, "coordinateOffsetX", frame);
			RWFloat offY = (RWFloat) RWUtil.getData(oldFrameListStruct, "coordinateOffsetY", frame);
			RWFloat offZ = (RWFloat) RWUtil.getData(oldFrameListStruct, "coordinateOffsetZ", frame);
			offX.setFloat(offX.getFloat()+x);
			offY.setFloat(offY.getFloat()+y);
			offZ.setFloat(offZ.getFloat()+z);
			frameStruct.addData("coordinateOffsetX", offX);
			frameStruct.addData("coordinateOffsetY", offY);
			frameStruct.addData("coordinateOffsetZ", offZ);
			//TODO: What to set here?
			frameStruct.addData("parentFrame", new RWInteger().setInteger(0));
			frameStruct.addData("unknown", new RWInteger().setInteger(0));
			
			//find and copy frame extension
			RWSection frameExt = RWUtil.getSection(oldFrameList, 0x0003, frame);
			frameList.addData("", frameExt);
			
			//find corresponding geometry
			RWSection oldGeomList = RWUtil.getSection(sec, 0x001A);
			RWSection geomSec = RWUtil.getSection(oldGeomList, 0x000F, geom);
			geomList.addData("", geomSec);
			
			pos++;
		}
	}
}
