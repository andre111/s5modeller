package me.andre111.s5modeller.util;

import me.andre111.s5modeller.rwbs.data.RWFloatArray2D;

public class MatUtil {
	public static RWFloatArray2D getRotMatrix(float angle, String axis) {
		RWFloatArray2D mat = new RWFloatArray2D(3, 3);
		
		if(axis.equals("X")) {
			mat.getArray()[0][0] = 1;
			mat.getArray()[0][1] = 0;
			mat.getArray()[0][2] = 0;

			mat.getArray()[1][0] = 0;
			mat.getArray()[1][1] = (float) Math.cos(angle);
			mat.getArray()[1][2] = (float) -Math.sin(angle);

			mat.getArray()[2][0] = 0;
			mat.getArray()[2][1] = (float) Math.sin(angle);
			mat.getArray()[2][2] = (float) Math.cos(angle);
		} else if(axis.equals("Y")) {
			mat.getArray()[0][0] = (float) Math.cos(angle);
			mat.getArray()[0][1] = 0;
			mat.getArray()[0][2] = (float) Math.sin(angle);

			mat.getArray()[1][0] = 0;
			mat.getArray()[1][1] = 1;
			mat.getArray()[1][2] = 0;

			mat.getArray()[2][0] = (float) -Math.sin(angle);
			mat.getArray()[2][1] = 0;
			mat.getArray()[2][2] = (float) Math.cos(angle);
		} else if(axis.equals("Z")) {
			mat.getArray()[0][0] = (float) Math.cos(angle);
			mat.getArray()[0][1] = (float) -Math.sin(angle);
			mat.getArray()[0][2] = 0;
			
			mat.getArray()[1][0] = (float) Math.sin(angle);
			mat.getArray()[1][1] = (float) Math.cos(angle);
			mat.getArray()[1][2] = 0;
			
			mat.getArray()[2][0] = 0;
			mat.getArray()[2][1] = 0;
			mat.getArray()[2][2] = 1;
		} else {
			throw new IllegalArgumentException("Axis can only be X,Y or Z!");
		}
		
		return mat;
	}
	
	public static RWFloatArray2D getScaleMatrix(float scaleX, float scaleY, float scaleZ) {
		RWFloatArray2D mat = new RWFloatArray2D(3, 3);
		
		mat.getArray()[0][0] = scaleX;
		mat.getArray()[0][1] = 0;
		mat.getArray()[0][2] = 0;
		
		mat.getArray()[1][0] = 0;
		mat.getArray()[1][1] = scaleY;
		mat.getArray()[1][2] = 0;
		
		mat.getArray()[2][0] = 0;
		mat.getArray()[2][1] = 0;
		mat.getArray()[2][2] = scaleZ;
		
		return mat;
	}
	
	public static RWFloatArray2D multiply(RWFloatArray2D first, RWFloatArray2D second) {
		//TODO Multiply matrixes
		RWFloatArray2D result = new RWFloatArray2D(3, 3);
		
		float[][] firstA = first.getArray();
		float[][] secondA = second.getArray();
		float[][] resultA = result.getArray();
		
		//[zeile][spalte]
		for (int i = 0; i < firstA.length; i++) { 
		    for (int j = 0; j < secondA[0].length; j++) { 
		        for (int k = 0; k < firstA[0].length; k++) { 
		        	resultA[i][j] += firstA[i][k] * secondA[k][j];
		        }
		    }
		}
		
		return result;
	}
	
	public static void setValues(RWFloatArray2D target, RWFloatArray2D source) {
		if(target.getArray().length!=source.getArray().length) throw new IllegalArgumentException("Array dimensions do not match!");
		
		for(int i=0; i<target.getArray().length; i++) {
			if(target.getArray()[i].length!=source.getArray()[i].length) throw new IllegalArgumentException("Array dimensions do not match!");
			for(int j=0; j<target.getArray()[i].length; j++) {
				target.getArray()[i][j] = source.getArray()[i][j];
			}
		}
	}
	
	public static float[] multiplyVector(RWFloatArray2D mat, float[] vector) {
		float[][] matA = mat.getArray();
		float[] resultA = new float[3];
		
		//[zeile][spalte]
		for(int i = 0; i < 3; i++) {
			float result = 0;
			for(int j = 0; j < matA[0].length; j++) {
				result += vector[j] * matA[i][j];
			}
			resultA[i] = result;
		}
		
		return resultA;
	}
	
	
	
	public static boolean checkSame(float[] v1, float[] v2) {
		if(v1.length!=v2.length) return false;

		float maxDist = 0.001f;
		for(int i=0; i<v1.length; i++) {
			float dist = Math.abs(v1[i]-v2[i]);
			if(dist>maxDist) return false;
		}

		return true;
	}
}
