#version 130

uniform sampler2D tex;


void main() {
    //final color: return opaque red
    gl_FragColor = texture(tex, gl_TexCoord[0].st);
}