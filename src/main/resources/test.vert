uniform float rotFactor;

void main() {
	gl_TexCoord[0] = gl_MultiTexCoord0;
	
	float s = sin(rotFactor * gl_Vertex.z);
	float c = cos(rotFactor * gl_Vertex.z);
	
	float rx = (c * gl_Vertex.x - s * gl_Vertex.y);
	float ry = (s * gl_Vertex.x + c * gl_Vertex.y);
	
	gl_Vertex.x = rx;
	gl_Vertex.y = ry;
	
    gl_Position = gl_ProjectionMatrix * gl_ModelViewMatrix * gl_Vertex;
}